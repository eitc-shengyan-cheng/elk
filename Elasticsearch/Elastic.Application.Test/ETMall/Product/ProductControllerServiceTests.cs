﻿using Elastic.Application.ETMall;
using Elastic.Application.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Elastic.Application.ETMall.Tests
{
    [TestClass()]
    public class ProductControllerServiceTests
    {
        protected Application.ETMall.ProductControllerService product = new Application.ETMall.ProductControllerService();

        [TestMethod()]
        public void SearchFor_Exist_Discount()
        {
            var request = new ProductSearchRequest()
            {
                Tokens = "手機",
                Account = "*unit_test"
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            request.Filter.Add("DISCOUNT_VALUE", "5");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            var data = product.SearchFor(request);

            Assert.IsNotNull(data);
            Assert.IsTrue(data.hits.total > 0);
        }

        [TestMethod()]
        public void SearchFor_Exist_OneDayShip()
        {
            var request = new ProductSearchRequest()
            {
                Tokens = "手機",
                Account = "*unit_test"
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            request.Filter.Add("ONEDAYSHIP", "1");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            var data = product.SearchFor(request);

            Assert.IsNotNull(data);
            Assert.IsTrue(data.hits.total > 0);
        }

        [TestMethod()]
        public void SearchFor_Exist_StoreService()
        {
            var request = new ProductSearchRequest()
            {
                Tokens = "手機",
                Account = "*unit_test"
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            request.Filter.Add("STORESERVICE", "1");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            var data = product.SearchFor(request);

            Assert.IsNotNull(data);
            Assert.IsTrue(data.hits.total > 0);
        }

        [TestMethod()]
        public void SearchFor_Exist_Coupon()
        {
            var request = new ProductSearchRequest()
            {
                Tokens = "手機",
                Account = "*unit_test"
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            request.Filter.Add("COUPON", "1");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            var data = product.SearchFor(request);

            Assert.IsNotNull(data);
            Assert.IsTrue(data.hits.total > 0);
        }

        [TestMethod()]
        public void SearchFor_NotExist()
        {
            var request = new ProductSearchRequest()
            {
                Tokens = "不存在的商品",
                Account = "*unit test"
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            request.Filter.Add("DISCOUNT_VALUE", "5");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            var data = product.SearchFor(request);

            Assert.IsNull(data);
        }

        [TestMethod()]
        public void SearchForId_Exist()
        {
            var arrage = "198929";
            var data = product.SearchForId(arrage);

            Assert.IsNotNull(data);
            Assert.IsNotNull(data._source);
            Assert.AreEqual(data._source.GOOD_ID, arrage);
        }

        [TestMethod()]
        public void SearchForId_NotExist()
        {
            var arrage = "000000";
            var data = product.SearchForId(arrage);

            Assert.IsNull(data);
        }
    }
}