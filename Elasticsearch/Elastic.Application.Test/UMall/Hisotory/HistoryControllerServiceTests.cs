﻿using Elastic.Application.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Elastic.Application.UMall.Tests
{
    [TestClass()]
    public class HistoryControllerServiceTests
    {
        protected Application.UMall.HistoryControllerService history = new Application.UMall.HistoryControllerService();

        [TestMethod()]
        public void GetUserBy_Exist()
        {
            var arrange = new HistoryRequest()
            {
                DateRangeGte = DateTime.Now.AddDays(-10),
                DateRangeLte = DateTime.Now.AddDays(1),
                Size = 50,
                User = "testing"
            };

            var result = this.history.GetUserBy(arrange);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.timed_out);
            Assert.IsTrue(result.took > 0);
            Assert.IsNotNull(result.aggregations);
            Assert.IsNotNull(result.aggregations.AggsWord);
            Assert.IsNotNull(result.aggregations.AggsWord.buckets);
            Assert.IsTrue(result.aggregations.AggsWord.buckets.Length > 0);
        }

        [TestMethod()]
        public void GetRankBy_Exist()
        {
            var arrange = new HistoryRequest()
            {
                DateRangeGte = DateTime.Now.AddDays(-10),
                DateRangeLte = DateTime.Now.AddDays(1),
                Size = 0,
                User = "testing"
            };

            var result = this.history.GetRankBy(arrange);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.timed_out);
            Assert.IsTrue(result.took > 0);
            Assert.IsNotNull(result.aggregations);
            Assert.IsNotNull(result.aggregations.AggsWord);
            Assert.IsNotNull(result.aggregations.AggsWord.buckets);
            Assert.IsTrue(result.aggregations.AggsWord.buckets.Length > 0);
        }

        [TestMethod()]
        public void InsertTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetSuggest_Exist()
        {
            var arrange = "電";

            var result = this.history.GetSuggestBy(arrange);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.product_suggest);
            Assert.IsTrue(result.product_suggest.Length > 0);
            Assert.IsTrue(result.product_suggest[0].text == arrange);
        }
    }
}