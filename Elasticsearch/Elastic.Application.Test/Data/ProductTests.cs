﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Elastic.Domain.UMall.Data.Tests
{
    [TestClass()]
    public class ProductTests
    {
        [TestMethod()]
        public void ConvertTest()
        {
            //arrange
            var productForParse = "6134888	【台北君悅酒店-凱菲屋】彩日本料理_平假日午晚餐券 2張	34954##52768##52770##52773	2700.00000	【台北君悅酒店-凱菲屋】彩日本料理_平假日午晚餐券 2張	False	986134888,票券,彩日本料理	6174278	1001	False			01746362	996134888	【台北君悅酒店-凱菲屋】彩日本料理_平假日午晚餐券 2張-OP-網		4	50509	1	2016-11-21 19:10:48.410000000	0	票券期限：即日起至 2017年8月31 日止	2	1		0";
            var expected = new
            {
                fugoSaleNo = "6134888",
                saleName = "【台北君悅酒店-凱菲屋】彩日本料理_平假日午晚餐券 2張",
                salePrice = 2700,
                isExpressProduct = 0,
                lastModifiedTime = DateTime.Parse("2016-11-21 19:10:48.410000000")
            };
            var target = new Product.Data.Product();

            //act
            var actual = Product.Data.Product.Convert(new string[] { productForParse });
            var product = actual.FirstOrDefault();

            //assert
            //expected.ToExpectedObject().ShouldMatch(product); //partial compare not working?
            Assert.AreEqual(expected.fugoSaleNo, product.FugoSaleNo);
            Assert.AreEqual(expected.saleName, product.GOOD_NM);
            Assert.AreEqual(expected.salePrice, product.PRC);
            Assert.AreEqual(expected.isExpressProduct, product.ONEDAYSHIP);
            Assert.AreEqual(expected.lastModifiedTime, product.lastModifiedTime);
        }
    }
}