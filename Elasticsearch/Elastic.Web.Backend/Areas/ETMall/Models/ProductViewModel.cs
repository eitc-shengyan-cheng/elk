﻿namespace Elastic.Web.Backend.Areas.ETMall.Models
{
    public class ProductViewModel
    {
        public string GOOD_ID { get; set; }
        public string PRC { get; set; }
        public string GOOD_NM { get; set; }
        public int COUPON { get; set; }
        public int DISCOUNT_VALUE { get; set; }
        public int STORESERVICE { get; set; }
        public int ONEDAYSHIP { get; set; }
        public int ONLINE { get; set; }
    }
}