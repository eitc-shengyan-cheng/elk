﻿using System.Web.Mvc;

namespace Elastic.Web.Backend.Areas.ETMall
{
    public class ETMallAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ETMall";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ETMall_default",
                "ETMall/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}