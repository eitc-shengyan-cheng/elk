﻿using Elastic.Application.DTO;
using Elastic.Application.ETMall;
using Elastic.Infra;
using Elastic.Web.Backend.Areas.ETMall.Models;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Elastic.Web.Backend.Areas.ETMall.Controllers
{
    public class ProductController : Controller
    {
        private IProductControllerService productRepository;
        private IProductRequestValidator requestValidator;

        public ProductController(IProductControllerService product, IProductRequestValidator validator)
        {
            this.productRepository = product;
            this.requestValidator = validator;
        }

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(string id)
        {
            var product = this.productRepository.SearchForId(id);

            if ( product == null )
                return RedirectToAction("Index");

            ProductViewModel model = MapperUtils.Map<ProductViewModel>(product._source);

            ViewBag.OneDayShip = new SelectList(
                items: new List<int>() { 0,1 },
                selectedValue: model.ONEDAYSHIP
            );

            ViewBag.Online = new SelectList(
                items: new List<int>() { 0, 1 },
                selectedValue: model.ONLINE
            );

            ViewBag.StoreService = new SelectList(
               items: new List<int>() { 0, 1 },
               selectedValue: model.STORESERVICE
            );

            ViewBag.Coupon = new SelectList(
               items: new List<int>() { 0, 1 },
               selectedValue: model.COUPON
            );
            
            return View(model);
        }
    }
}