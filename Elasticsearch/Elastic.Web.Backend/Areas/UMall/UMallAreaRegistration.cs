﻿using System.Web.Mvc;

namespace Elastic.Web.Backend.Areas.UMall
{
    public class UMallAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "UMall";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "UMall_default",
                "UMall/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}