﻿using Elastic.Application;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Elastic.Web.Backend
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var di = new SimpleInjector.Container();

            if (IndexProductSetting.GetSite() == "etmall")
            {
                di.Register<Elastic.Application.ETMall.IProductControllerService, Elastic.Application.ETMall.ProductControllerService>();
                di.Register<Elastic.Application.DTO.IProductRequestValidator, Elastic.Application.ETMall.Product.ProductRequestValidator>();

                di.Register<Elastic.Application.ETMall.IHistoryControllerService, Elastic.Application.ETMall.HistoryControllerService>();
                di.Register<Elastic.Application.DTO.IHistoryRequestValidator, Elastic.Application.ETMall.History.HistoryRequestValidator>();
            }
            else
            {
                di.Register<Elastic.Application.UMall.IProductControllerService, Elastic.Application.UMall.ProductControllerService>();
                di.Register<Elastic.Application.DTO.IProductRequestValidator, Elastic.Application.UMall.Product.ProductRequestValidator>();

                di.Register<Elastic.Application.UMall.IHistoryControllerService, Elastic.Application.UMall.HistoryControllerService>();
                di.Register<Elastic.Application.DTO.IHistoryRequestValidator, Elastic.Application.UMall.History.HistoryRequestValidator>();
            }

            di.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(di));
        }
    }
}
