﻿
namespace Elastic.Infra
{
    public class MethodResult<T> : BasicResult<T>
    {
        public string Name { get; set; }

        public string Parameter { get; set; }

        public dynamic Command { get; set; }

        public bool HasError { get; set; }


        public MethodResult()
        {
            this.HasError = false;
            this.Message = string.Empty;
            this.Data = default(T);
            this.Name = string.Empty;
            this.Parameter = string.Empty;
        }
    }
}
