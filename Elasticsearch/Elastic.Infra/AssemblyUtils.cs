﻿using System.Diagnostics;
using System.Reflection;

namespace Elastic.Infra
{
    public static class AssemblyUtils
    {
        public static FileVersionInfo GetVersion()
        {
            var a = Assembly.GetExecutingAssembly();
            return FileVersionInfo.GetVersionInfo(a.Location);
        }
    }
}
