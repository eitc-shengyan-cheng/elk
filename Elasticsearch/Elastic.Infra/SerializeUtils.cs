﻿namespace Elastic.Infra
{   
        /// <summary>
        /// ToDo + NLOG
        /// </summary>
    public static class SerializeUtils
    {
        public static T Deserialize<T>(string data)
        {
            return NetJSON.NetJSON.Deserialize<T>(data);
        }

        public static string Serialize<T>(T data)
        {
            return NetJSON.NetJSON.Serialize<T>(data);
        }

        public static string Serialize(string data)
        {
            return NetJSON.NetJSON.Serialize(data);
        }

        public static string SerializeDynamic(string data)
        {
            return Jil.JSON.SerializeDynamic(data);
        }

        public static dynamic DeserializeDynamic(string data)
        {
            return Jil.JSON.DeserializeDynamic(data);
        }

        public static bool IsPropertyExist(string data, string propertyName)
        {
            if (data == string.Empty)
                return false;

            dynamic r = Jil.JSON.DeserializeDynamic(data);
            return r.ContainsKey(propertyName);
        }

        public static bool TryParseProperty(string data, string propertyName, out string result)
        {
            result = string.Empty;
            if (data == string.Empty)
                return false;

            dynamic r = Jil.JSON.DeserializeDynamic(data);
            if (r.ContainsKey(propertyName))
            {
                result = r[propertyName].ToString();
                return true;
            }
            return false;
        }
    }
}
