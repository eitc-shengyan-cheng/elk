﻿using Nelibur.ObjectMapper;

namespace Elastic.Infra
{
    public static class MapperUtils
    {
        public static X Map<X>(object source)
        {
            return TinyMapper.Map<X>(source);
        }
    }
}
