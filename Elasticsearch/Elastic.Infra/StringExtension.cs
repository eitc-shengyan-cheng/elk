﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elastic.Infra
{
    public static class StringExtension
    {
        public static int ParseInt(this String data)
        {
            if (string.IsNullOrEmpty(data)) return 0;

            int result = 0;
            if (int.TryParse(data, out result))
                return result;
            else
                return 0;
        }

        public static int ParseBool(this String data)
        {
            if (string.IsNullOrEmpty(data)) return 0;

            if (data.ToLower() == "true")
                return 1;
            else
                return 0;
        }

        public static double ParseDouble(this String data)
        {
            if (string.IsNullOrEmpty(data)) return 0;

            double result = 0;
            if (double.TryParse(data, out result))
                return result;
            else
                return 0;
        }
    }
}
