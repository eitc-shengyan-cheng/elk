﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Caching;

namespace Elastic.Infra
{
    /// <summary>
    /// http://www.cnblogs.com/luminji/archive/2011/09/08/2169955.html
    /// 在Add方法中，有一个条件判断，必须做出这样的处理，否则缓存机制将会缓存第一次的结果，过了有效期后缓存讲失效并不再重建；
    /// 在示例程序中，我们简单的将缓存放到了Cache目录下，在实际的项目实践中，考虑到缓存的页面将是成千上万的，所以我们必须要做目录分级，否则寻找并读取缓存文件将会成为效率瓶颈，这会耗尽CPU。
    /// </summary>
    public class FileCacheProvider : OutputCacheProvider
    {
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void Initialize(string name, NameValueCollection attributes)
        {
            base.Initialize(name, attributes);
            CachePath = HttpContext.Current.Server.MapPath(attributes["cachePath"]);
        }

        public override object Add(string key, object entry, DateTime utcExpiry)
        {
            Object obj = Get(key);
            if (obj != null)     
            {
                return obj;
            }
            Set(key, entry, utcExpiry);
            return entry;
        }

        public override object Get(string key)
        {
            string path = ConvertKeyToPath(key);
            if (!File.Exists(path))
            {
                return null;
            }
            CacheItem item = null;
            using (FileStream file = File.OpenRead(path))
            {
                var formatter = new BinaryFormatter();
                item = (CacheItem)formatter.Deserialize(file);
            }

            if (item.ExpiryDate <= DateTime.Now.ToUniversalTime())
            {
                Remove(key);
                return null;
            }
            return item.Item;
        }


        public override void Set(string key, object entry, DateTime utcExpiry)
        {
            CacheItem item = new CacheItem(entry, utcExpiry);
            string path = ConvertKeyToPath(key);
            using (FileStream file = File.OpenWrite(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, item);
            }
        }

        public override void Remove(string key)
        {
            string path = ConvertKeyToPath(key);
            if (File.Exists(path))
                File.Delete(path);
        }

        public string CachePath
        {
            get;
            set;
        }

        private string ConvertKeyToPath(string key)
        {
            string file = key.Replace('/', '-');
            file += ".txt";
            return Path.Combine(CachePath, file);
        }
    }

    [Serializable]
    public class CacheItem
    {
        public DateTime ExpiryDate;
        public object Item;

        public CacheItem(object entry, DateTime utcExpiry)
        {
            Item = entry;
            ExpiryDate = utcExpiry;
        }
    }
}
