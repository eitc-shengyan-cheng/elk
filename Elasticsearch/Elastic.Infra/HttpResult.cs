﻿using System.Net;

namespace Elastic.Infra
{
    public class HttpResult<T> : BasicResult<T>
    {
        public HttpStatusCode StatusCode { get; set; }
    }
}
