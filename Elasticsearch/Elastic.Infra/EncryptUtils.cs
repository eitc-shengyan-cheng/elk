﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Elastic.Infra
{
    public class EncryptUtils
    {
        /// <summary>
        /// http://www.cnblogs.com/catcher1994/p/5677011.html
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetEncrypt(string data, string key)
        {
            HMACMD5 source = new HMACMD5(Encoding.UTF8.GetBytes(key));
            byte[] buff = source.ComputeHash(Encoding.UTF8.GetBytes(data));
            string result = string.Empty;
            for (int i = 0; i < buff.Length; i++)
            {
                result += buff[i].ToString("X2"); // hex format
            }
            return result;
        }

        private static ulong Hash(DateTime when)
        {
            ulong kind = (ulong)(int)when.Kind;
            return (kind << 62) | (ulong)when.Ticks;
        }
    }
}
