﻿using System.Collections.Generic;

namespace Elastic.Infra
{
    public static class ListExtension
    {

        public static string ToArrayText(this IList<string> list)
        {
            var count = 0;
            string s = "[ ";

            for (int i = 0; i < list.Count; i++)
            {
                if (count < (list.Count - 1))
                    s += string.Format("\"{0}\",", list[i].ToString());
                else
                    s += string.Format("\"{0}\" ", list[i].ToString());
                count++;
            }

            return s + " ]";
        }
    }
}
