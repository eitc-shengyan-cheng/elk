﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;

namespace Elastic.Infra
{
    public static class DynamicUtils
    {
        public static dynamic ConvertToDynamic(object obj)
        {
            IDictionary<string, object> result = new ExpandoObject();
            
            foreach(PropertyDescriptor pro in TypeDescriptor.GetProperties(obj.GetType()))
            {
                result.Add(pro.Name, pro.GetValue(obj));
            }

            return result as ExpandoObject;
        }
    }
}
