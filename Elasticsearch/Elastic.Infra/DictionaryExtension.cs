﻿using System.Collections.Generic;

namespace Elastic.Infra
{
    public static class DictionaryExtension
    {
        public static string ToArrayPairText(this IDictionary<string, string> list)
        {
            var count = 0;
            string s = "[ ";

            foreach (KeyValuePair<string, string> kv in list)
            {
                if (count < (list.Count - 1))
                    s += string.Format("{{\"{0}\": \"{1}\"}}, ", kv.Key, kv.Value);
                else
                    s += string.Format("{{\"{0}\": \"{1}\"}}", kv.Key, kv.Value);
                count++;
            }

            return s + " ]";
        }

        public static string ToPairText(this IDictionary<string, string> list, string prefix, string pass = "")
        {
            string s = string.Empty;

            foreach (KeyValuePair<string, string> kv in list)
            {
                if ( kv.Key.ToLower() != pass.ToLower())
                s += "{ \"" + prefix + "\": { \"" + kv.Key + "\": \"" + kv.Value + "\" } },";
            }

            return s;
        }
    }
}
