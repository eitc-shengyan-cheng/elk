﻿
namespace Elastic.Infra
{
    public class BasicResult<T>
    {
        public string Message { get; set; }

        public T Data { get; set; }

        public BasicResult()
        {
            this.Message = string.Empty;
            this.Data = default(T);
        }
    }
}
