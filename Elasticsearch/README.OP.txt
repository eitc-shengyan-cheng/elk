*.Sever Role & Folder

--STAGE
192.168.57.15		[Client, Master/Data, Website]
192.168.57.19		[Client, Master/Data]

--PROD
10.35.1.81			[Client, Master/Data, Website, Data Job]
10.35.1.82			[Client, Master/Data, Website]

D:\ELK				[Elasticsearch Nodes]
D:\ELK\ES.Data		[Data and Log of Nodes]
D:\ELK\ES.Plugins	[Plugin of Nodes]
D:\ELK\ES920X		[One of Nodes]

D:\ELK_FILE			[相關必要檔案]	
D:\ELK_WEB			[Search API Service]
D:\ELK_Console		[Daily Job for Import Product Data]
					EU目前設定為09:00和0910來執行排程

*.DEPLOY
手動建立 Elk.Cache FOLDER
WEB.CONFIG
	10.35.1.81
	ETMALL & UMALL <add key="ELK_ElasticsearchUrl" value="http://10.35.1.81:9200/" />

	10.35.1.82 
	ETMALL & UMALL <add key="ELK_ElasticsearchUrl" value="http://10.35.1.82:9201/" />

*.相關URL

++STAGE 15,19
--Dashboard & Plugin
http://192.168.57.15:9200/_plugin/kopf
http://192.168.57.15:9200/_plugin/head

--Monitor & APM
http://127.0.0.1:2012/
http://192.168.57.15/elmah
http://192.168.57.15:81/elmah

--UMLL 模擬頁,API URL,自我測試URL
http://192.168.57.15/portal
http://192.168.57.15/umall/product/Search
http://192.168.57.15/umall/product?debug=1 
http://192.168.57.19/portal
http://192.168.57.19/umall/product/Search

--ETMLL 模擬頁,API URL,自我測試URL
http://192.168.57.15:81/portal
http://192.168.57.15:81/etmall/product/Search
http://192.168.57.15:81/etmall/product?debug=1
http://192.168.57.19:81/portal
http://192.168.57.19:81/etmall/product/Search

++PROD 81,82 
--Dashboard & Plugin
http://10.35.1.81:9200/_plugin/kopf
http://10.35.1.81:9200/_plugin/head
http://10.35.1.82:9201/_plugin/kopf
http://10.35.1.82:9201/_plugin/head

--Monitor & APM
http://127.0.0.1:2012/
http://10.35.1.81/elmah
http://10.35.1.81:81/elmah
http://10.35.1.82/elmah
http://10.35.1.82:81/elmah

--UMALL 模擬頁,API URL,自我測試URL
http://10.35.1.81/portal
http://10.35.1.81/umall/product/Search
http://10.35.1.81/umall/product?debug=1
http://10.35.1.82/portal
http://10.35.1.82/umall/product/Search
http://10.35.1.82/umall/product?debug=1

--ETMALL 模擬頁,API URL,自我測試URL
http://10.35.1.81:81/portal
http://10.35.1.81:81/etmall/product/Search
http://10.35.1.81:81/etmall/product?debug=1
http://10.35.1.82:81/portal
http://10.35.1.82:81/etmall/product/Search
http://10.35.1.82:81/etmall/product?debug=1

*.Acccount
sch_adminZaq12wsx
