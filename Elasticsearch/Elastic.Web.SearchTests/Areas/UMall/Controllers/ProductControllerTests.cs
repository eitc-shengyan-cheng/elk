﻿using Elastic.Application.DTO;
using Elastic.Application.UMall;
using Elastic.Application.UMall.Product;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Elastic.Web.Search.Areas.UMall.Controllers.Tests
{
    [TestClass()]
    public class ProductControllerTests
    {
        protected ProductController controller = new ProductController(new ProductControllerService(), new ProductRequestValidator());

        [TestMethod()]
        public void UpdateById_Success()
        {
            var arrange = new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "5936807",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            };

            var result = controller.UpdateById(arrange);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Data.ToString().Contains("successful"));
        }

        [TestMethod()]
        public void UpdateById_NotExist()
        {
            var arrange = new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "0000000",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            };

            var result = controller.UpdateById(arrange);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Data.ToString() == arrange.GOOD_ID);
        }

        [TestMethod()]
        public void Updated_Success()
        {
            var arrange = new Application.DTO.ProductUpdateRequest<int>();
            arrange.Items = new List<ProductUpdateRequestItem<int>>();

            arrange.Items.Add(new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "5936807",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            });

            arrange.Items.Add(new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "5936807",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            });

            var result = controller.Update(arrange);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }

        [TestMethod()]
        public void Updated_Success_Half()
        {
            var arrange = new Application.DTO.ProductUpdateRequest<int>();
            arrange.Items = new List<ProductUpdateRequestItem<int>>();

            arrange.Items.Add(new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "5936807",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            });

            arrange.Items.Add(new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "0000000",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            });

            arrange.Items.Add(new Application.DTO.ProductUpdateRequestItem<int>()
            {
                GOOD_ID = "5936355",
                COUPON = 1,
                DISCOUNT_VALUE = 1,
                GOOD_NM = "【鍋寶】3人份電子鍋 D&#45;RCO&#45;3002",
                ONEDAYSHIP = 1,
                PRC = 1,
                STORESERVICE = 1
            });

            var result = controller.Update(arrange);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }
    }
}