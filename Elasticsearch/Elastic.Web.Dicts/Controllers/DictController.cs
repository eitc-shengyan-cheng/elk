﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Elastic.Web.Dicts.Controllers
{
    public class DictController : Controller
    {
        [OutputCache(CacheProfile = "MyCache")]
        public ActionResult Index()
        {
            //Response.Cache.SetOmitVaryStar(true);
            // string localFile = @"D:\ELK\ES.Plugins\client.master\elasticsearch-analysis-ik-1.10.1\config\custom\mydict.dic";
            string localFile = @"D:\ELK\ES.Plugins\client.master\elasticsearch-analysis-ik-1.10.1\config\custom\mydict.dic";
            List<string> list = System.IO.File.ReadAllLines(localFile).ToList();

            var model = System.IO.File.ReadAllText(localFile);
 

            //response.Headers.Add("ETag", GetEncrypt(DateTime.Now.ToString(), "platstar"));
            return Content(model, "text/plain", Encoding.UTF8);
        }
        
        [OutputCache(Duration = 60, VaryByParam = "none")]
        public ActionResult Test()
        {
            // string localFile = @"D:\ELK\ES.Plugins\client.master\elasticsearch-analysis-ik-1.10.1\config\custom\mydict.dic";
            string localFile = @"D:\ELK\ES.Plugins\client.master\elasticsearch-analysis-ik-1.10.1\config\custom\mydict.dic";
            List<string> list = System.IO.File.ReadAllLines(localFile).ToList();

            var model = System.IO.File.ReadAllText(localFile);


            //response.Headers.Add("ETag", GetEncrypt(DateTime.Now.ToString(), "platstar"));
            return Content(model, "text/plain", Encoding.UTF8);
        }
 
    }
}