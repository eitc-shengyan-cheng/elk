﻿using Elastic.Application;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Elastic.Web.Search
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // 應用程式啟動時執行的程式碼
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            GlobalConfiguration.Configuration.Formatters.Add(GlobalConfiguration.Configuration.Formatters.JsonFormatter);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var di = new SimpleInjector.Container();

            if (IndexProductSetting.GetSite() == "etmall")
            {
                di.Register<Elastic.Application.ETMall.IProductControllerService, Elastic.Application.ETMall.ProductControllerService>();
                di.Register<Elastic.Application.DTO.IProductRequestValidator, Elastic.Application.ETMall.Product.ProductRequestValidator>();

                di.Register<Elastic.Application.ETMall.IHistoryControllerService, Elastic.Application.ETMall.HistoryControllerService>();
                di.Register<Elastic.Application.DTO.IHistoryRequestValidator, Elastic.Application.ETMall.History.HistoryRequestValidator>();                
            }
            else
            {
                di.Register<Elastic.Application.UMall.IProductControllerService, Elastic.Application.UMall.ProductControllerService>();
                di.Register<Elastic.Application.DTO.IProductRequestValidator, Elastic.Application.UMall.Product.ProductRequestValidator>();

                di.Register<Elastic.Application.UMall.IHistoryControllerService, Elastic.Application.UMall.HistoryControllerService>();
                di.Register<Elastic.Application.DTO.IHistoryRequestValidator, Elastic.Application.UMall.History.HistoryRequestValidator>();                
            }

            di.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(di));
        }
    }
}