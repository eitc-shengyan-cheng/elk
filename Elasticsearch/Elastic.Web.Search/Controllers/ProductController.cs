﻿using Elastic.Application.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Elastic.Web.Search.Controllers
{
    /// <summary>
    /// http://localhost:5946/api/product
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductController : ApiController
    {
        /// <summary>
        ///  for load test
        /// </summary>
        /// <returns></returns>
        public Task<Domain.UMall.Product.Search.ProductResponse> Get()
        {
            ProductSearchRequest request = new ProductSearchRequest()
            {
                Tokens = DateTime.Now.Millisecond.ToString()
                //Tokens = new List<string>() { "PCHOME" },               
            };

            request.Filter = new Dictionary<string, string>();
            //request.Filter.Add("ONLINE", "1");
            //request.Filter.Add("CategoryStore", "視聽家電");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 10000;

            Elastic.Application.UMall.ProductControllerService product = new Elastic.Application.UMall.ProductControllerService();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            return Task.Run(() =>
            {

                for (int i = 0; i <= 50000 ; i++)
                {
                   product.SearchFor(request);
                }


                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                Console.WriteLine("RunTime " + elapsedTime);


                return product.SearchFor(request);
            });

        }

        /// <summary>
        /// 依輸入的字詞搜尋
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<Domain.UMall.Product.Search.ProductResponse> Post(ProductSearchRequest command)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Elastic.Application.UMall.ProductControllerService product = new Elastic.Application.UMall.ProductControllerService();
            
            return Task.Run(() =>
            {
                return product.SearchForAsync(command);
            });
        }
    }
}