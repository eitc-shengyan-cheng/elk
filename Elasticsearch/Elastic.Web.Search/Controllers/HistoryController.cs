﻿using Elastic.Application.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;

namespace Elastic.Web.Search.Controllers
{
    public class HistoryController : ApiController
    {
        /// <summary>
        ///  for testing
        /// </summary>
        /// <returns></returns>
        public async Task<Domain.ETMall.Product.Search.ProductResponse> Get()
        {
            ProductSearchRequest request = new ProductSearchRequest()
            {
                Tokens = "日本 澎湖現撈海草蝦急",
                //Tokens = new List<string>() { "PCHOME" },               
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            //request.Filter.Add("CategoryStore", "視聽家電");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 1;
            request.PriceRangeLte = 50000;

            Elastic.Application.ETMall.ProductControllerService product = new Elastic.Application.ETMall.ProductControllerService();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i <= 20; i++)
            {
                await product.SearchForAsync(request);
            }

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            return await product.SearchForAsync(request);
        }

        /// <summary>
        /// 依輸入的字詞搜尋
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task<Domain.ETMall.Product.Search.ProductResponse> Post(ProductSearchRequest command)
        {
            Elastic.Application.ETMall.ProductControllerService product = new Elastic.Application.ETMall.ProductControllerService();
            return await product.SearchForAsync(command); 
        }
    }
}