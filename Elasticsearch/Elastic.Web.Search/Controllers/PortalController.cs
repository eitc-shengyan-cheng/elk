﻿using Elastic.Infra;
using System.Diagnostics;
using System.Reflection;
using System.Web.Mvc;

namespace Elastic.Web.Search.Controllers
{
    /// <summary>
    /// 模擬B2C
    /// </summary>
    public class PortalController : Controller
    {
        // GET: Portal
        public ActionResult Index()
        {
            return View();
        }

        public string Version()
        {
            return typeof(PortalController).Assembly.GetName().Version.ToString();
            
        }
    }
}


 