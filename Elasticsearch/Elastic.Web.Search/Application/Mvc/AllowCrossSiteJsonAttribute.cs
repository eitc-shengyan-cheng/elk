﻿using System;
using System.Web.Mvc;

namespace Elastic.Web.Search
{
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute 
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            /*
            var controllerName = filterContext.RouteData.Values["controller"];
            var actionName = filterContext.RouteData.Values["action"];
            var message = String.Format("{0} controller:{1} action:{2}", "onactionexecuting", controllerName, actionName);

            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            */
            base.OnActionExecuting(filterContext);
        } 
    }
}