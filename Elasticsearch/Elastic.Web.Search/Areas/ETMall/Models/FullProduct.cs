﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elastic.Web.Search.Areas.ETMall.Models
{
    public class FullProduct
    {
        public string GOOD_ID { get; set; }
        public string GOOD_NM { get; set; }
        public string Specifications { get; set; }
        public int PRC { get; set; }
    }
}