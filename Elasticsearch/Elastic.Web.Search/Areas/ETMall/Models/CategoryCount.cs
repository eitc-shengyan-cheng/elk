﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elastic.Web.Search.Areas.ETMall.Models
{
    public class CategoryCount
    {
        public string Source { get; set; }
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public int Count { get; set; }
    }
}