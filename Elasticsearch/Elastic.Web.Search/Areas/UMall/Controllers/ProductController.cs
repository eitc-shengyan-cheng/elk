﻿using Elastic.Application.DTO;
using Elastic.Application.UMall;
using Elastic.Application.Utils;
using Elastic.Infra;
using Elastic.Web.Search.Areas.UMall.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Elastic.Web.Search.Areas.UMall.Controllers
{
    /// <summary>
    /// 取得商品
    /// http://localhost:5946/umall/product
    /// </summary>
    public class ProductController : Controller
    {
        private IProductControllerService productRepository;
        private IProductRequestValidator requestValidator;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ProductController(IProductControllerService product, IProductRequestValidator validator)
        {
            this.productRepository = product;
            this.requestValidator = validator;
        }

        /// <summary>
        ///  for testing
        /// </summary>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public JsonResult Index()
        {
            try
            {
                var data = this.Request.QueryString["debug"];
                if (data != null)
                {
                    ProductSearchRequest request = new ProductSearchRequest()
                    {
                        Tokens = "空氣清淨機",
                        Account = "*self_test"
                    };

                    request.Filter = new Dictionary<string, string>();
                    request.Filter.Add("ONLINE", "1");
                    request.Filter.Add("DISCOUNT_VALUE", "5");

                    request.Sort = new Dictionary<string, string>();
                    request.Sort.Add("GOOD_NM", "desc");
                    request.Sort.Add("DISCOUNT_VALUE", "desc");

                    request.PriceRangeGte = 0;
                    request.PriceRangeLte = 50000;
                    return new JsonResult() { Data = this.Search(request), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                else
                {
                    return new JsonResult() { Data = System.Net.Dns.GetHostName(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = System.Net.Dns.GetHostName(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        /// <summary>
        /// 依輸入的字詞搜尋
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        [OutputCache(CacheProfile = "MyCache")]
        public JsonResult Search(ProductSearchRequest request)
        {
            try
            {
                if (this.requestValidator.IsValid(request))
                {
                    if (this.requestValidator.IsGoodIdFormat(request.Tokens))
                    {
                        return this.SearchId(request.Tokens);
                    }
                    else
                    {
                        Elastic.Domain.UMall.Product.Search.ProductResponse array = this.productRepository.SearchFor(request);
                        var result = new ProductResult<Elastic.Web.Search.Areas.UMall.Models.Product>(request, array, HttpContextUtils.PhysicalApplicationPath());
                        result.Pages = PagingUtils.GetPageIndex(result.Count, request.PageSize);
                        return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    }
                }
                else
                {
                    return new JsonResult() { Data = request, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = request, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        [OutputCache(CacheProfile = "MyCache")]
        public async Task<ActionResult> SearchAsync(ProductSearchRequest request)
        {
            try
            {
                if (this.requestValidator.IsValid(request))
                {
                    var data = await this.productRepository.SearchForAsync(request);
                    var result = new ProductResult<Elastic.Web.Search.Areas.UMall.Models.Product>(request, data, HttpContextUtils.PhysicalApplicationPath());
                    result.Pages = PagingUtils.GetPageIndex(result.Count, request.PageSize);
                    return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                else
                {
                    return new JsonResult() { Data = request, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = request, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [AllowCrossSiteJson]
        [ValidateInput(false)]
        [OutputCache(CacheProfile = "MyCache", VaryByParam = "productid")]
        public JsonResult SearchId(string productid)
        {
            try
            {
                if (!string.IsNullOrEmpty(productid))
                {
                    Elastic.Domain.UMall.Product.Search.ProductSingleResponse single = this.productRepository.SearchForId(productid);
                    var result = new ProductResult<Elastic.Domain.UMall.Product.Data.Product>(single);
                    return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                else
                {
                    return new JsonResult() { Data = productid, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = productid, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public JsonResult UpdateById(ProductUpdateRequestItem<int> request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.GOOD_ID))
                    return new JsonResult() { Data = request.GOOD_ID, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

                Elastic.Domain.UMall.Product.Search.ProductSingleResponse single = this.productRepository.SearchForId(request.GOOD_ID);

                if (single == null)
                    return new JsonResult() { Data = request.GOOD_ID, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

                var product = MapperUtils.Map<Elastic.Domain.UMall.Product.Data.Product>(single._source);
                product.PRC = request.PRC;
                product.DISCOUNT_VALUE = request.DISCOUNT_VALUE;
                product.STORESERVICE = request.STORESERVICE;
                product.ONEDAYSHIP = request.ONEDAYSHIP;
                product.COUPON = request.COUPON;
                product.GOOD_NM = request.GOOD_NM;
                product.ONLINE = request.ONLINE;

                MethodResult<string> result = this.productRepository.UpdateFor(product);
                return new JsonResult() { Data = result.Data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = request.GOOD_ID, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public JsonResult Update(ProductUpdateRequest<int> request)
        {
            try
            {
                var message = request.Items.Count.ToString();

                for (int i = 0; i < request.Items.Count; i++)
                {
                    Elastic.Domain.UMall.Product.Search.ProductSingleResponse single = this.productRepository.SearchForId(request.Items[i].GOOD_ID);

                    if (single == null || single._source == null)
                    {
                        message += string.Format(",{0},empty", request.Items[i].GOOD_ID);
                        continue;
                    }

                    var product = MapperUtils.Map<Elastic.Domain.UMall.Product.Data.Product>(single._source);
                    product.PRC = request.Items[i].PRC;
                    product.DISCOUNT_VALUE = request.Items[i].DISCOUNT_VALUE;
                    product.STORESERVICE = request.Items[i].STORESERVICE;
                    product.ONEDAYSHIP = request.Items[i].ONEDAYSHIP;
                    product.COUPON = request.Items[i].COUPON;
                    product.GOOD_NM = request.Items[i].GOOD_NM;
                    product.ONLINE = request.Items[i].ONLINE;

                    MethodResult<string> result = this.productRepository.UpdateFor(product);
                    message += string.Format(",{0},{1}", request.Items[i].GOOD_ID, result.Message);                    
                }
                return new JsonResult() { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet }; 
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = request.Items[0].GOOD_ID, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public JsonResult Suggest(string command)
        {
            try
            {
                var data = this.productRepository.GetSuggestBy(command);

                if (data == null)
                    return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                else
                    return new JsonResult() { Data = data.product_suggest[0].options.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public void RemoveCache(string action)
        {
            var url = Url.Action(action, "product");
            HttpContext.Response.RemoveOutputCacheItem(url);

            HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(System.Web.HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();
        }

        /// <summary>
        /// test
        /// </summary>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public JsonResult get()
        {
            return this.SearchId("5936355");
        }

        /// <summary>
        /// test
        /// </summary>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public void modify()
        {            
            //this.UpdatePriceById(new ProductUpdateRequest<int>() { Data = 20005, Id = "5936355" });
        }
    }
}