﻿using Elastic.Application.DTO;
using Elastic.Application.UMall;
using NLog;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Elastic.Web.Search.Areas.UMall.Controllers
{
    public class HistoryController : Controller
    {
        private IHistoryControllerService historyRepository;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public HistoryController(IHistoryControllerService history)
        {
            this.historyRepository = history;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        public JsonResult Index()
        {
            try
            {
                var data = this.Request.QueryString["debug"];
                if (data == null) return new JsonResult() { Data = DateTime.Now, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

                HistoryRequest request = new HistoryRequest()
                {
                    DateRangeGte = DateTime.Now.AddDays(-1),
                    DateRangeLte = DateTime.Now,
                    Size = 10,
                    User = "testing"
                };

                //return this.Account(request);
                return this.Rank(request);    
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = DateTime.Now, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        /// <summary>
        /// 依使用者帳號取得查詢過的關鍵字
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        [OutputCache(CacheProfile = "MyCache")]
        public JsonResult Account(HistoryRequest command)
        {
            try
            {
                var data = this.historyRepository.GetUserBy(command);

                if (data == null || data.hits.total == 0)
                    return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                else
                    return new JsonResult() { Data = data.hits.hits.Select(x => x._source.Word).ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        /// <summary>
        /// 取得查詢過的關鍵字排行榜
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        [OutputCache(CacheProfile = "MyCache")]
        public JsonResult Rank(HistoryRequest command)
        {
            try
            {
                var data = this.historyRepository.GetRankBy(command);

                if (data == null )
                    return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                else
                    return new JsonResult() { Data = data.aggregations.AggsWord.buckets.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        /// <summary>
        /// 取得建議關鍵字 by prefix
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [AllowCrossSiteJson]
        [ValidateInput(false)]
        [OutputCache(CacheProfile = "MyCache")]
        public JsonResult Suggest(string command)
        {
            try
            {
                var data = this.historyRepository.GetSuggestBy(command);

                if (data == null)
                    return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                else
                    return new JsonResult() { Data = data.product_suggest[0].options.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                logger.Fatal(Elastic.Application.Utils.LogUtils.BuildExceptionMessage(ex));
                return new JsonResult() { Data = "No Match Rows", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}