﻿using Elastic.Application;
using Elastic.Application.DTO;
using Elastic.Domain.UMall.Product.Search;
using Elastic.Infra;
using System;
using System.Collections;
using System.Linq;

namespace Elastic.Web.Search.Areas.UMall.Models
{
    public class ProductResult<T> : IProductResponse
    {
        public ProductResult(ProductSingleResponse response)
        {
            this.Info = new ExtensionInfo()
            {
                S = System.Net.Dns.GetHostName(),
                ES = ElasticSetting.Host,
                Took = 0
            };

            this.Count = 1;
            this.Pages = 1;
            this.Result = MapperUtils.Map<T>(response._source);
        }

        public ProductResult(ProductSearchRequest request, ProductResponse response, string filePath)
        {
            var isNull = (response == null || response.hits == null);

            this.Info = new ExtensionInfo()
            {
                S = System.Net.Dns.GetHostName(),
                ES = ElasticSetting.Host,
                Took = (isNull) ? 0 : response.took
            };

            this.Count = (isNull) ? 0 : response.hits.total;
            this.Result = response.hits.hits.Select(x => MapperUtils.Map<T>(x._source));
            this.Categorys = (isNull) ? null : GetCategoryCount(response.aggregations.CategoryStoreAGGS.buckets, request.CategoryID, request.CategoryLevel, filePath);
        }

        public CategoryCount[] GetCategoryCount(Bucket[] buckets, int categoryID, int level, string filePath)
        {
            if (CategorySetting.Categorys == null) CategorySetting.GetCategorys(filePath);
            var levelCategoryIds = CategorySetting.Categorys.Where(x => x.Value.LEVEL == level && x.Value.PARENT_ID == categoryID).Select(x => x.Key.ToString());

            return buckets.Where(x => levelCategoryIds.Contains(x.key)).Select(x => new CategoryCount { CategoryID = Int32.Parse(x.key), Count = x.doc_count, Name = CategorySetting.Categorys[Int32.Parse(x.key)].CATE_NAME, Source = "Category" }).ToArray();
        }

        public IList Categorys { get; set; }

        public string[] Terms { get; set; }

        public int Pages { get; set; }

        public IList Predefined { get; set; }

        public dynamic Result { get; set; }

        public int Code { get; set; }

        public string Desc { get; set; }

        public int Count { get; set; }

        public ExtensionInfo Info { get; set; }
    }
}
