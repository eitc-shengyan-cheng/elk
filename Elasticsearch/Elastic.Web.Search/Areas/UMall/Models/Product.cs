﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elastic.Web.Search.Areas.UMall.Models
{
    public class Product
    {
        public string GOOD_ID { get; set; }
        public string GOOD_NM { get; set; }
        public string Specifications { get; set; }
        public double PRC { get; set; }
        public int DISCOUNT_VALUE { get; set; }
    }
}