﻿
var site = 'umall';
var vm = new Vue({

    el: '#app',
    data: {
        newSearch: {
            KeyNote: 'NAUTICA',
            CategoryStore: '',
            CategoryID: 0,
            CategoryLevel: 0,
            productSource: 'default',
            PriceRangeGte: '0',
            PriceRangeLte: '500000',
            OneDayShip: '',
            StoreService: '',
            COUPON: '',
            DISCOUNT_VALUE: '',
            sort: '_score',
            AdultProduct: '',
            TravelProduct: '',
            ONLINE: '1',


        },
        request: {},
        resultData: {},
        gridData: [],
        categoryData: [],
        apiUrl: 'http://localhost:5946/'+site+'/product/Search'
    },
    methods: {

        createCustomer: function () {

            var filtering = {}
            if (this.newSearch.ONLINE !== undefined && this.newSearch.ONLINE != "") {
                filtering.ONLINE = this.newSearch.ONLINE;
            }

            if (this.newSearch.CategoryStore !== undefined && this.newSearch.CategoryStore != "") {
                this.newSearch.CategoryID = parseInt(this.newSearch.CategoryStore);
                filtering.CategoryValue = this.newSearch.CategoryStore;
            }
            else {
                this.newSearch.CategoryID = 0;
                this.newSearch.CategoryLevel = 0;
            }
            if (this.newSearch.productSource == "TravelProduct") {
                filtering.TravelProduct = 1;
            }
            if (this.newSearch.productSource == "AdultProduct") {
                filtering.AdultProduct = 1;
            }


            if (this.newSearch.OneDayShip == true) {
                filtering.OneDayShip = '1';
            }
            if (this.newSearch.StoreService == true) {
                filtering.StoreService = '1';
            }
            if (this.newSearch.COUPON == true) {
                filtering.COUPON = '1';
            }
            if (this.newSearch.DISCOUNT_VALUE == true) {
                filtering.DISCOUNT_VALUE = '1';
            }

            var keyword = this.newSearch.KeyNote;
            var sorting;
            if (this.newSearch.sort == "PRC, desc") {
                sorting = { "PRC": "desc" };
            } else if (this.newSearch.sort == "PRC, asc") {
                sorting = { "PRC": "asc" };
            } else {
                sorting = { [this.newSearch.sort]: "desc" };
            }

            var requestData = {
                Tokens: keyword,
                Filter: filtering,
                Sort: sorting,
                PageIndex: 1,
                PageSize: 40,
                PriceRangeGte: this.newSearch.PriceRangeGte,
                PriceRangeLte: this.newSearch.PriceRangeLte,
                CategoryLevel: this.newSearch.CategoryLevel,
                CategoryID: this.newSearch.CategoryID
            };

            var vm = this
            vm.gridData = [];
            vm.resultData = {};
            vm.categoryData = [];


            console.log(requestData);
            vm.$http.post(vm.apiUrl, requestData)
                .then((response) => {

                    vm.resultData.PageTotalCount = response.data.Pages;
                    vm.resultData.TotalCount = response.data.Count;
                    vm.resultData.Spent = Math.floor(response.data.Info.Took);
                    vm.resultData.ES = response.data.Info.ES;
                    vm.resultData.S = response.data.Info.S;

                    vm.gridData = (JSON.parse(response.body).Result);
                    vm.categoryData = (JSON.parse(response.body).Categorys);
                    console.log(response);
                })
                .catch(function (response) {
                    if (response.ok == false) {
                        console.log(response.body);
                    } else {
                        console.log('response', response);
                    }
                })

        },
        selectCategory: function (category) {
            this.newSearch.CategoryStore = category;
            this.newSearch.CategoryLevel += 1;
            this.createCustomer();
        }
    },
    mounted: function(){
        this.apiUrl = this.$root.$el.ownerDocument.origin + '/' + site + '/product/Search'
        

    }
})

var suggester = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: '/' + site + '/product/Suggest?command=%QUERY',
        wildcard: '%QUERY'
    }
});

$('.typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
}, 
{
    name: 'auto-completion',
    display: 'text',
    source: suggester,
    limit: 10
});

$('.typeahead').on('typeahead:selected', function (evt, item) {
    console.log(item);
    vm.newSearch.KeyNote = item.text;
});