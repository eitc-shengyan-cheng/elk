﻿using System.Collections.Generic;

namespace Elastic.Domain
{
    public interface IProductRepository
    {
        string GetSearchText(string token, List<string> fields, IDictionary<string, string> filter, IDictionary<string, string> sorts, int priceGte, int priceLte, int categoryId, IList<string> source, int from, int size);

        string GetBulkInsertDocumentJson(string indexName, string typeName, string id, string item);

        string GetSuggestJson(string text); 
    }
}
