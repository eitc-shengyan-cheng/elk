﻿using System.Collections.Generic;
using System.Dynamic;

namespace Elastic.Domain.ETMall.Product.Search
{
    public class ProductRequest
    {
        public Aggs aggs { get; set; }
        public Query query { get; set; }
        public IDictionary<string, string> sort { get; set; }
    }

    public class Aggs
    {
        public CategorystoreAggs CategoryStoreAGGS { get; set; }
    }

    public class CategorystoreAggs
    {
        public Terms terms { get; set; }
    }

    public class Terms
    {
        public string field { get; set; }
        public int size { get; set; }
    }

    public class Query
    {
        public Bool @bool { get; set; }
    }

    public class Bool
    {
        public Must must { get; set; }
        public List<Filter> filter { get; set; }
    }

    public class Must
    {
        public Multi_Match multi_match { get; set; }
    }

    public class Multi_Match
    {
        public string query { get; set; }
        public string type { get; set; }
        public List<string> fields { get; set; }
        public string @operator { get; set; }
    }

    public class Filter
    {
        public dynamic match { get; set; }

        public Range range { get; set; }
    }

    public class Range
    {
        public DISCOUNT_VALUE DISCOUNT_VALUE { get; set; }
    }

    public class DISCOUNT_VALUE
    {
        public int gte { get; set; }
        public int lte { get; set; }
    }

    public class Match : DynamicObject
    {
        private IDictionary<string, object> _values;

        public Match(IDictionary<string, object> values)
        {
            _values = values;
        }
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (_values.ContainsKey(binder.Name))
            {
                result = _values[binder.Name];
                return true;
            }
            result = null;
            return false;
        }
    }
}