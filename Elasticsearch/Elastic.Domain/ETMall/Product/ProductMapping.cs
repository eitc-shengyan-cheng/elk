﻿namespace Elastic.Domain.ETMall.Product.Mapping
{
    public class ProductMapping
    {
        /// <summary>
        /// Member Name 對應ES TYPE NAME
        /// </summary>
        public Product product { get; set; }
    }
    public class Product
    {
        public _All _all { get; set; }
        public Properties properties { get; set; }
    }

    public class _All
    {
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string term_vector { get; set; }
        public string store { get; set; }
        public string enabled { get; set; }
    }

    public class Properties
    {
        public GOOD_ID GOOD_ID { get; set; }
        public GOOD_NM GOOD_NM { get; set; }
        public CategoryStore2 CategoryStore2 { get; set; }
        public PRC PRC { get; set; }
        public Specifications Specifications { get; set; }
        public Description Description { get; set; }
        public BUNDLE_CD BUNDLE_CD { get; set; }
        public KEYWORD KEYWORD { get; set; }
        public ONLINE ONLINE { get; set; }
        public PRODUCTSOURCE PRODUCTSOURCE { get; set; }
        public DISCOUNT_VALUE DISCOUNT_VALUE { get; set; }
        public Categorystore CategoryStore { get; set; }
        public Categorylclass CategoryLClass { get; set; }
        public Categorymclass CategoryMClass { get; set; }
        public Categorysclass CategorySClass { get; set; }
        public Travelproduct TravelProduct { get; set; }
        public Adultproduct AdultProduct { get; set; }
        public Fugosaleno FugoSaleNo { get; set; }
        public APP_DISCOUNT_VALUE APP_DISCOUNT_VALUE { get; set; }
        public MOBI_DISCOUNT_VALUE MOBI_DISCOUNT_VALUE { get; set; }
        public Product_Suggest Product_Suggest { get; set; }
        public CategoryValue CategoryValue { get; set; }
    }

    public class GOOD_ID
    {
        public string type { get; set; }
        public string index { get; set; }
    }

    public class GOOD_NM
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class CategoryStore2
    {
        public string type { get; set; }
        public string index { get; set; }

    }

    public class PRC
    {
        public string type { get; set; }
    }

    public class Specifications
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class Description
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class BUNDLE_CD
    {
        public string type { get; set; }
    }

    public class KEYWORD
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class ONLINE
    {
        public string type { get; set; }

    }

    public class PRODUCTSOURCE
    {
        public string type { get; set; }
    }

    public class DISCOUNT_VALUE
    {
        public string type { get; set; }
    }

    public class Categorystore
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class Categorylclass
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class Categorymclass
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class Categorysclass
    {
        public string type { get; set; }
        public string store { get; set; }
        public string term_vector { get; set; }
        public string analyzer { get; set; }
        public string search_analyzer { get; set; }
        public string include_in_all { get; set; }
        public int boost { get; set; }
    }

    public class Travelproduct
    {
        public string type { get; set; }
    }

    public class Adultproduct
    {
        public string type { get; set; }
    }

    public class Fugosaleno
    {
        public string type { get; set; }
    }

    public class APP_DISCOUNT_VALUE
    {
        public string type { get; set; }
    }

    public class MOBI_DISCOUNT_VALUE
    {
        public string type { get; set; }

    }
    public class StoreService
    {
        public string type { get; set; }
    }

    public class Product_Suggest
    {
        public int max_input_length { get; set; }
        public bool payloads { get; set; }
        public string analyzer { get; set; }
        public bool preserve_position_increments { get; set; }
        public string type { get; set; }
        public bool preserve_separators { get; set; }
    }

    public class CategoryValue
    {
        public string type { get; set; }
    }
}