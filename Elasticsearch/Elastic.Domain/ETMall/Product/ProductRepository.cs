﻿using Elastic.Infra;
using System.Collections.Generic;
using System.Text;

namespace Elastic.Domain.ETMall.Product
{
    public class ProductRepository : IProductRepository
    {

        public string GetSearchText(string token, List<string> fields, IDictionary<string, string> filter, IDictionary<string, string> sorts, int priceGte, int priceLte, int categoryId, IList<string> source, int from, int size)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" {    \t\n");
            sb.Append("      \"aggs\": { \"CategoryStoreAGGS\": { \"terms\": { \"field\": \"CategoryValue\", \"size\": 1000  }  }  }    \t\n");
            sb.Append("      ,\"sort\": " + sorts.ToArrayPairText() + " \t\n");
            sb.Append("      ,\"query\":{ \"bool\": {  \"must\": { \"multi_match\": { \"query\": \"" + token + "\", \"type\": \"cross_fields\",  \t\n");
            sb.Append("         \"fields\": " + fields.ToArrayText() + " ,   \t\n");
            sb.Append("         \"operator\": \"and\" } }, \"filter\": [ " + filter.ToPairText("match", "DISCOUNT_VALUE") + " \t\n");

            if (filter.ContainsKey("DISCOUNT_VALUE"))
                sb.Append("         { \"range\": { \"DISCOUNT_VALUE\": { \"gte\": " + priceGte + ", \"lte\": " + priceLte + " } } },   \t\n");

            sb.Append("         { \"range\": { \"PRC\": { \"gte\": " + priceGte + ", \"lte\": " + priceLte + " } } }   \t\n");
            sb.Append("     ] } }    \t\n");

            sb.Append("     ,\"highlight\": { \"pre_tags\": [ \"<span class='marked'>\" ], \"post_tags\": [ \"</span>\" ],  \t\n");
            sb.Append("         \"fields\": {   \t\n");
            sb.Append("             \"GOOD_NM\": { \"term_vector\" : \"with_positions_offsets\" },    \t\n");
            sb.Append("             \"Description\": { \"term_vector\" : \"with_positions_offsets\" },  \t\n");
            sb.Append("             \"Specifications\": { \"term_vector\" : \"with_positions_offsets\" }    \t\n");
            sb.Append("   } }   \t\n");
            sb.Append("      ,\"from\": " + from.ToString() + " ,\"size\": " + size.ToString() + " ,\"_source\": " + source.ToArrayText() + " \t\n");

            sb.Append(" } \t\n");

            return sb.ToString();
        }

       
        #region
        /// <summary>
        /// 以MultiMatch語法搜尋(暫不使用)
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public Elastic.Domain.ETMall.Product.Search.ProductRequest GetSearchJson(string token, List<string> fields, IDictionary<string, string> filter, IDictionary<string, string> sort, int priceGte, int priceLte)
        {
            Product.Search.ProductRequest root = new Product.Search.ProductRequest();
            root.sort = sort;
            root.aggs = new Product.Search.Aggs();
            root.aggs.CategoryStoreAGGS = new Product.Search.CategorystoreAggs();
            root.aggs.CategoryStoreAGGS.terms = new Product.Search.Terms();
            root.aggs.CategoryStoreAGGS.terms.field = "CategoryStore2";
            root.aggs.CategoryStoreAGGS.terms.size = 10;

            root.query = new Product.Search.Query();
            root.query.@bool = new Product.Search.Bool();
            root.query.@bool.must = new Product.Search.Must();
            root.query.@bool.must.multi_match = new Product.Search.Multi_Match();
            root.query.@bool.must.multi_match.query = token;
            root.query.@bool.must.multi_match.type = "cross_fields";
            root.query.@bool.must.multi_match.@operator = "and";
            root.query.@bool.must.multi_match.fields = new List<string>();

            for (int i = 0; i < fields.Count; i++)
            {
                root.query.@bool.must.multi_match.fields.Add(fields[i]);
            }

            root.query.@bool.filter = new List<Product.Search.Filter>();
            /*
            root.query.@bool.filter.Add(new Search.Filter()
            {
                range = new Search.Range()
                {
                    DISCOUNT_VALUE = new Search.DISCOUNT_VALUE()
                    {
                        gte = priceGte,
                        lte = priceLte
                    }
                }
            });
            */
            foreach (KeyValuePair<string, string> entry in filter)
            {
                var match_filter = new Dictionary<string, object>();
                match_filter.Add(entry.Key, entry.Value);
                root.query.@bool.filter.Add(new Product.Search.Filter()
                {
                    match = match_filter
                    /*,
                    range = new Search.Range()
                    {
                        DISCOUNT_VALUE = new Search.DISCOUNT_VALUE()
                        {
                            gte = priceGte,
                            lte = priceLte
                        }
                    }*/
                });
            }

            return root;
        }
        #endregion

        /// <summary>
        /// 取得用來設定商品INDEX的MAPPING
        /// </summary>
        /// <returns></returns>
        public Elastic.Domain.ETMall.Product.Mapping.ProductMapping GetTypeMappingJson()
        {
            Elastic.Domain.ETMall.Product.Mapping.ProductMapping mapping = new Elastic.Domain.ETMall.Product.Mapping.ProductMapping();
            mapping.product = new Elastic.Domain.ETMall.Product.Mapping.Product();
            mapping.product._all = new Elastic.Domain.ETMall.Product.Mapping._All();
            mapping.product._all.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product._all.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product._all.term_vector = IndexMappingEnum.term_vector_no;
            mapping.product._all.store = IndexMappingEnum.store;
            mapping.product._all.enabled = "false";

            mapping.product.properties = new Elastic.Domain.ETMall.Product.Mapping.Properties();
            mapping.product.properties.GOOD_ID = new Elastic.Domain.ETMall.Product.Mapping.GOOD_ID();
            mapping.product.properties.GOOD_ID.type = IndexMappingEnum.type_string;
            mapping.product.properties.GOOD_ID.index = IndexMappingEnum.Not_Analyzed;

            mapping.product.properties.GOOD_NM = new Elastic.Domain.ETMall.Product.Mapping.GOOD_NM();
            mapping.product.properties.GOOD_NM.type = IndexMappingEnum.type_string;
            mapping.product.properties.GOOD_NM.store = IndexMappingEnum.store;
            mapping.product.properties.GOOD_NM.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.GOOD_NM.analyzer = IndexMappingEnum.Analyzer_ik_max_word;
            mapping.product.properties.GOOD_NM.search_analyzer = IndexMappingEnum.Analyzer_ik_max_word;
            mapping.product.properties.GOOD_NM.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.GOOD_NM.boost = IndexMappingEnum.boost;

            mapping.product.properties.CategoryStore2 = new Elastic.Domain.ETMall.Product.Mapping.CategoryStore2();
            mapping.product.properties.CategoryStore2.type = IndexMappingEnum.type_string;
            mapping.product.properties.CategoryStore2.index = IndexMappingEnum.Not_Analyzed;

            mapping.product.properties.PRC = new Elastic.Domain.ETMall.Product.Mapping.PRC();
            mapping.product.properties.PRC.type = IndexMappingEnum.type_int;

            mapping.product.properties.Specifications = new Elastic.Domain.ETMall.Product.Mapping.Specifications();
            mapping.product.properties.Specifications.type = IndexMappingEnum.type_string;
            mapping.product.properties.Specifications.store = IndexMappingEnum.store;
            mapping.product.properties.Specifications.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.Specifications.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Specifications.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Specifications.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.Specifications.boost = IndexMappingEnum.boost;

            mapping.product.properties.Description = new Elastic.Domain.ETMall.Product.Mapping.Description();
            mapping.product.properties.Description.type = IndexMappingEnum.type_string;
            mapping.product.properties.Description.store = IndexMappingEnum.store;
            mapping.product.properties.Description.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.Description.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Description.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Description.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.Description.boost = IndexMappingEnum.boost;

            mapping.product.properties.BUNDLE_CD = new Elastic.Domain.ETMall.Product.Mapping.BUNDLE_CD();
            mapping.product.properties.BUNDLE_CD.type = IndexMappingEnum.type_int;

            mapping.product.properties.KEYWORD = new Elastic.Domain.ETMall.Product.Mapping.KEYWORD();
            mapping.product.properties.KEYWORD.type = IndexMappingEnum.type_string;
            mapping.product.properties.KEYWORD.store = IndexMappingEnum.store;
            mapping.product.properties.KEYWORD.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.KEYWORD.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.KEYWORD.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.KEYWORD.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.KEYWORD.boost = IndexMappingEnum.boost;

            mapping.product.properties.ONLINE = new Elastic.Domain.ETMall.Product.Mapping.ONLINE();
            mapping.product.properties.ONLINE.type = IndexMappingEnum.type_int;

            mapping.product.properties.PRODUCTSOURCE = new Elastic.Domain.ETMall.Product.Mapping.PRODUCTSOURCE();
            mapping.product.properties.PRODUCTSOURCE.type = IndexMappingEnum.type_int;

            mapping.product.properties.DISCOUNT_VALUE = new Elastic.Domain.ETMall.Product.Mapping.DISCOUNT_VALUE();
            mapping.product.properties.DISCOUNT_VALUE.type = IndexMappingEnum.type_int;

            mapping.product.properties.CategoryStore = new Elastic.Domain.ETMall.Product.Mapping.Categorystore();
            mapping.product.properties.CategoryStore.type = IndexMappingEnum.type_string;
            mapping.product.properties.CategoryStore.store = IndexMappingEnum.store;
            mapping.product.properties.CategoryStore.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.CategoryStore.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategoryStore.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategoryStore.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.CategoryStore.boost = IndexMappingEnum.boost;

            mapping.product.properties.CategoryLClass = new Elastic.Domain.ETMall.Product.Mapping.Categorylclass();
            mapping.product.properties.CategoryLClass.type = IndexMappingEnum.type_string;
            mapping.product.properties.CategoryLClass.store = IndexMappingEnum.store;
            mapping.product.properties.CategoryLClass.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.CategoryLClass.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategoryLClass.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategoryLClass.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.CategoryLClass.boost = IndexMappingEnum.boost;

            mapping.product.properties.CategoryMClass = new Elastic.Domain.ETMall.Product.Mapping.Categorymclass();
            mapping.product.properties.CategoryMClass.type = IndexMappingEnum.type_string;
            mapping.product.properties.CategoryMClass.store = IndexMappingEnum.store;
            mapping.product.properties.CategoryMClass.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.CategoryMClass.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategoryMClass.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategoryMClass.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.CategoryMClass.boost = IndexMappingEnum.boost;

            mapping.product.properties.CategorySClass = new Elastic.Domain.ETMall.Product.Mapping.Categorysclass();
            mapping.product.properties.CategorySClass.type = IndexMappingEnum.type_string;
            mapping.product.properties.CategorySClass.store = IndexMappingEnum.store;
            mapping.product.properties.CategorySClass.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.CategorySClass.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategorySClass.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.CategorySClass.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.CategorySClass.boost = IndexMappingEnum.boost;

            mapping.product.properties.TravelProduct = new Elastic.Domain.ETMall.Product.Mapping.Travelproduct();
            mapping.product.properties.TravelProduct.type = IndexMappingEnum.type_int;

            mapping.product.properties.AdultProduct = new Elastic.Domain.ETMall.Product.Mapping.Adultproduct();
            mapping.product.properties.AdultProduct.type = IndexMappingEnum.type_int;

            mapping.product.properties.FugoSaleNo = new Elastic.Domain.ETMall.Product.Mapping.Fugosaleno();
            mapping.product.properties.FugoSaleNo.type = IndexMappingEnum.type_int;

            mapping.product.properties.APP_DISCOUNT_VALUE = new Elastic.Domain.ETMall.Product.Mapping.APP_DISCOUNT_VALUE();
            mapping.product.properties.APP_DISCOUNT_VALUE.type = IndexMappingEnum.type_int;

            mapping.product.properties.MOBI_DISCOUNT_VALUE = new Elastic.Domain.ETMall.Product.Mapping.MOBI_DISCOUNT_VALUE();
            mapping.product.properties.MOBI_DISCOUNT_VALUE.type = IndexMappingEnum.type_int;

            mapping.product.properties.Product_Suggest = new Mapping.Product_Suggest();
            mapping.product.properties.Product_Suggest.max_input_length = 50;
            mapping.product.properties.Product_Suggest.payloads = false;
            mapping.product.properties.Product_Suggest.analyzer = "simple";
            mapping.product.properties.Product_Suggest.preserve_position_increments = true;
            mapping.product.properties.Product_Suggest.type = "completion";
            mapping.product.properties.Product_Suggest.preserve_separators = true;

            mapping.product.properties.CategoryValue = new Elastic.Domain.ETMall.Product.Mapping.CategoryValue();
            mapping.product.properties.CategoryValue.type = IndexMappingEnum.type_string;

            return mapping;
        }

        public string GetBulkInsertDocumentJson(string indexName, string typeName, string id, string item)
        {
            return "{ \"create\": { \"_index\": \"" + indexName + "\",   \"_type\": \"" + typeName + "\",  \"_id\": \"" + id + "\"  } } \t\n" + item + " \t\n";
        }

        public string GetBulkInsertDocumentText(string indexName, string typeName, string id, Product.Data.Product item)
        {
            return "{ \"create\": { \"_index\": \"" + indexName + "\",   \"_type\": \"" + typeName + "\",  \"_id\": \"" + id + "\"  } } \t\n" + "{ \"MOBI_DISCOUNT_VALUE\": \"" + item.MOBI_DISCOUNT_VALUE + "\",    \"APP_DISCOUNT_VALUE\": \"" + item.APP_DISCOUNT_VALUE + "\",    \"FugoSaleNo\": \"" + item.FugoSaleNo + "\",    \"AdultProduct\": \"" + item.AdultProduct + "\",    \"TravelProduct\": \"" + item.TravelProduct + "\",    \"CategorySClass\": \"" + item.CategorySClass + "\",    \"CategoryMClass\": \"" + item.CategoryMClass + "\",    \"CategoryLClass\": \"" + item.CategoryLClass + "\",    \"CategoryStore2\": \"" + item.CategoryStore2 + "\",   \"CategoryStore\": \"" + item.CategoryStore + "\",    \"DISCOUNT_VALUE\": \"" + item.DISCOUNT_VALUE + "\",    \"COUPON\": \"" + item.COUPON + "\",    \"HOTSALE\": \"" + item.HOTSALE + "\",    \"HOTVIEW\": \"" + item.HOTVIEW + "\",    \"PRODUCTSOURCE\": \"" + item.PRODUCTSOURCE + "\",    \"ONLINE\": \"" + item.ONLINE + "\",    \"KEYWORD\": \"" + item.KEYWORD + "\",    \"BUNDLE_CD\": \"" + item.BUNDLE_CD + "\",    \"OneDayShip\": \"" + item.ONEDAYSHIP + "\",    \"Description\": \"" + item.Description + "\",   \"Specifications\": \"" + item.Specifications + "\",    \"PRC\": \"" + item.PRC + "\",    \"GOOD_NM\": \"" + item.GOOD_NM + "\",    \"GOOD_ID\": \"" + item.GOOD_ID + "\"  } \t\n";

        }
        
        public string GetSuggestJson(string text)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" { ");
            sb.Append("   \"product_suggest\": { ");
            sb.Append("     \"text\": \"" + text + "\", ");
            sb.Append("     \"completion\": { ");
            sb.Append("       \"field\": \"Product_Suggest\" ");
            sb.Append("     } ");
            sb.Append("   } ");
            sb.Append(" } ");

            return sb.ToString();
        }
    }
}