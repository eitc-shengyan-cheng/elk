﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elastic.Domain.ETMall.Product.Search.Suggest
{
    public class ProductSuggestResponse
    {
        public _Shards _shards { get; set; }
        public Product_Suggest[] product_suggest { get; set; }
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Product_Suggest
    {
        public string text { get; set; }
        public int offset { get; set; }
        public int length { get; set; }
        public Option[] options { get; set; }
    }

    public class Option
    {
        public string text { get; set; }
        public double score { get; set; }
    }

}