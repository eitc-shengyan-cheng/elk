﻿using Elastic.Infra;
using System.Collections.Generic;
using System.Linq;

namespace Elastic.Domain.ETMall.Product.Data
{
    /// <summary>
    /// 匯入資料的 Schema
    /// CLASS名稱必須和Elasticsearch TYPE相同
    /// </summary>
    public class Product
    {
        public string GOOD_ID;
        public string GOOD_NM;
        public int PRC;
        public string Specifications;
        public string Description;
        public int ONEDAYSHIP;
        public int BUNDLE_CD;
        public string KEYWORD;
        public int ONLINE;
        public int PRODUCTSOURCE;

        public int HOTVIEW;
        public int HOTSALE;
        public int COUPON;

        public int DISCOUNT_VALUE;
        public string CategoryStore;
        /// <summary>
        /// 用在統計各館符合數量
        /// for aggs
        /// </summary>
        public string CategoryStore2; 
        public string CategoryLClass;
        public string CategoryMClass;
        public string CategorySClass;
        public int TravelProduct;
        public int AdultProduct;
        public int FugoSaleNo;
        public int APP_DISCOUNT_VALUE;
        public int MOBI_DISCOUNT_VALUE;
        public int STORESERVICE;
        public string CategoryValue;
        public Product_Suggest Product_Suggest;

        /// <summary>
        /// 將原始資料轉換成要匯入ES的資料型別
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IEnumerable<Product> Convert(IEnumerable<string> data)
        {
            return data.Select(x => x.Split('\t')).Skip(1).Select(f => new Product
            {
                GOOD_ID = f[0],
                GOOD_NM = f[1],
                PRC = f[2].ParseInt(),
                Specifications = f[3],
                Description = f[4],
                ONEDAYSHIP = f[5].ParseInt(),
                BUNDLE_CD = f[6].ParseInt(),
                KEYWORD = f[7],
                ONLINE = f[8].ParseInt(),
                PRODUCTSOURCE = f[9].ParseInt(),
                HOTVIEW = f[10].ParseInt(),
                HOTSALE = f[11].ParseInt(),
                COUPON = f[12].ParseInt(),
                DISCOUNT_VALUE = f[13].ParseInt(),
                CategoryStore = f[14],
                CategoryStore2 = f[14],
                CategoryLClass = f[15],
                CategoryMClass = f[16],
                CategorySClass = f[17],
                TravelProduct = f[18].ParseInt(),
                AdultProduct = f[19].ParseInt(),
                FugoSaleNo = f[20].ParseInt(),
                APP_DISCOUNT_VALUE = f[21].ParseInt(),
                MOBI_DISCOUNT_VALUE = f[22].ParseInt(),
                STORESERVICE = f[23].ParseInt(),
                CategoryValue = f[24],
                Product_Suggest = new Product_Suggest()
                {
                    input = f[7].Split(' ')
                }
            });
        }
    }

    public class Product_Suggest
    {
        public string[] input { get; set; }
    }
}