﻿namespace Elastic.Domain.ETMall.Product.Search
{
    public class ProductSingleResponse
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public bool found { get; set; }
        public ProductSingleResponseSource _source { get; set; }
    }

    public class ProductSingleResponseSource
    {
        public int MOBI_DISCOUNT_VALUE { get; set; }
        public int APP_DISCOUNT_VALUE { get; set; }
        public int FugoSaleNo { get; set; }
        public int AdultProduct { get; set; }
        public int TravelProduct { get; set; }
        public int DISCOUNT_VALUE { get; set; }
        public int COUPON { get; set; }
        public int HOTSALE { get; set; }
        public int HOTVIEW { get; set; }
        public int PRODUCTSOURCE { get; set; }
        public int ONLINE { get; set; }
        public int BUNDLE_CD { get; set; }
        public int OneDayShip { get; set; }
        public int PRC { get; set; }
        public string CategorySClass { get; set; }
        public string CategoryMClass { get; set; }
        public string CategoryLClass { get; set; }
        public string CategoryStore2 { get; set; }
        public string CategoryStore { get; set; }
        public string KEYWORD { get; set; }
        public string Description { get; set; }
        public string Specifications { get; set; }
        public string GOOD_NM { get; set; }
        public string GOOD_ID { get; set; }
        public string CategoryValue { get; set; }
        public int StoreService { get; set; }
    }
}
