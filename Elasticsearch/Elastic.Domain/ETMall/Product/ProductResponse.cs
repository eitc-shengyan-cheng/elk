﻿using System;

namespace Elastic.Domain.ETMall.Product.Search
{
    public class ProductResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public _Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Aggregations aggregations { get; set; }
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public Hit[] hits { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public _Source _source { get; set; }
        public Highlight highlight { get; set; }
        public object[] sort { get; set; }
    }

    public class _Source
    {
        public int MOBI_DISCOUNT_VALUE { get; set; }
        public int APP_DISCOUNT_VALUE { get; set; }
        public int FugoSaleNo { get; set; }
        public int AdultProduct { get; set; }
        public int TravelProduct { get; set; }
        public int DISCOUNT_VALUE { get; set; }
        public int COUPON { get; set; }
        public int HOTSALE { get; set; }
        public int HOTVIEW { get; set; }
        public int PRODUCTSOURCE { get; set; }
        public int ONLINE { get; set; }
        public int BUNDLE_CD { get; set; }
        public int OneDayShip { get; set; }
        public int PRC { get; set; }
        public string CategorySClass { get; set; }
        public string CategoryMClass { get; set; }
        public string CategoryLClass { get; set; }
        public string CategoryStore2 { get; set; }
        public string CategoryStore { get; set; }
        public string KEYWORD { get; set; }
        public string Description { get; set; }
        public string Specifications { get; set; }
        public string GOOD_NM { get; set; }
        public string GOOD_ID { get; set; }
        public string CategoryValue { get; set; }
        public int StoreService { get; set; }
    }

    public class Highlight
    {
        public string[] GOOD_NM { get; set; }
        public string[] Description { get; set; }
        public string[] Specifications { get; set; }
    }

    public class Aggregations
    {
        public Categorystoreaggs CategoryStoreAGGS { get; set; }
    }

    public class Categorystoreaggs
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public Bucket[] buckets { get; set; }
    }

    public class Bucket
    {
        public string key { get; set; }
        public int doc_count { get; set; }
    }
}