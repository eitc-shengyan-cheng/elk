﻿namespace Elastic.Domain.ETMall.History.Search.Rank
{
    public class HistoryRankResponse
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public _Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Aggregations aggregations { get; set; }
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public Hit[] hits { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public _Source _source { get; set; }
        public string[] sort { get; set; }
    }

    public class _Source
    {
        public string Account { get; set; }
        public object Word { get; set; }
        public int Count { get; set; }
        public string Date { get; set; }
        public Json Json { get; set; }
        public Word_Suggest Word_Suggest { get; set; }
    }

    public class Json
    {
        public object Account { get; set; }
        public string Tokens { get; set; }
        public int PriceRangeGte { get; set; }
        public int PriceRangeLte { get; set; }
        public Filter Filter { get; set; }
        public Sort Sort { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class Filter
    {
        public string ONLINE { get; set; }
    }

    public class Sort
    {
        public string GOOD_NM { get; set; }
        public string DISCOUNT_VALUE { get; set; }
    }

    public class Word_Suggest
    {
        public string[] input { get; set; }
    }

    public class Aggregations
    {
        public Aggsword AggsWord { get; set; }
    }

    public class Aggsword
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public Bucket[] buckets { get; set; }
    }

    public class Bucket
    {
        public string key { get; set; }
        public int doc_count { get; set; }
    }

}



