﻿namespace Elastic.Domain.ETMall.History.Search.Suggest
{
    public class HistorySuggestResponse
    {
        public _Shards _shards { get; set; }
        public Product_Suggest[] product_suggest { get; set; }
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Product_Suggest
    {
        public string text { get; set; }
        public int offset { get; set; }
        public int length { get; set; }
        public Option[] options { get; set; }
    }

    public class Option
    {
        public string text { get; set; }
        public double score { get; set; }
    }

}
