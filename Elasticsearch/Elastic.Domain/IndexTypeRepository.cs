﻿namespace Elastic.Domain
{
    public class IndexTypeRepository
    {
        public string GetIndexSettingJson(string indexName)
        {
            return "{ \"settings\": { \"index\": { \"number_of_shards\": 3, \"number_of_replicas\": 1 } } }";
        }

        public string GetAliasesSettingJson(string aliaseName, string removeIndex, string appendIndex)
        {
            return "{ \"actions\": [    { \"remove\": { \"index\": \"" + removeIndex + "\",\"alias\": \"" + aliaseName + "\"  }},    { \"add\": {  \"index\": \"" + appendIndex + "\",	\"alias\": \"" + aliaseName + "\" }  }  ]  }";
        }

        public string GetRemoveAliasesSettingJson(string aliaseName, string removeIndex)
        {
            return "{ \"actions\": [    { \"remove\": { \"index\": \"" + removeIndex + "\",\"alias\": \"" + aliaseName + "\"  }}  ]  }";
        }

        public string GetCreateAliasesSettingJson(string aliaseName, string appendIndex)
        {
            return "{ \"actions\": [   { \"add\": {  \"index\": \"" + appendIndex + "\",	\"alias\": \"" + aliaseName + "\" }  }  ]  }";
        }

        public string GetTypeRefreshSetting(string seconds, string durability, string ops)
        {
            return "{  \"refresh_interval\":\"" + seconds + "s\" , \"index.translog.durability\": \"" + durability + "\",  \"index.translog.flush_threshold_ops\": \"" + ops + "\" } ";
        }
    }
}
