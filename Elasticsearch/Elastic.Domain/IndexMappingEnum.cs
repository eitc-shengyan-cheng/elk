﻿namespace Elastic.Domain
{
    public static class IndexMappingEnum
    {
        public const string Analyzer_ik_smart = "ik_smart";
        public const string Analyzer_standard = "standard";
        public const string Analyzer_ik_max_word = "ik_max_word";
        public const string Not_Analyzed = "not_analyzed";
        public const string term_vector = "with_positions_offsets";
        public const string term_vector_no = "no";
        public const string store = "false";
        public const string type_string = "string";
        public const string type_long = "long";
        public const string type_int = "integer";
        public const string include_in_all = "false";
        public const int boost = 8;
    }
}
