﻿using Elastic.Infra;
using System.Collections.Generic;
using System.Text;

namespace Elastic.Domain.UMall.Product
{
    public class ProductRepository : IProductRepository
    {
        public string GetSearchText(string token, List<string> fields, IDictionary<string, string> filter, IDictionary<string, string> sorts, int priceGte, int priceLte, int categoryId, IList<string> source, int from, int size)
        {         
            StringBuilder sb = new StringBuilder();

            sb.Append(" {    \t\n");
            sb.Append("      \"aggs\": { \"CategoryStoreAGGS\": { \"terms\": { \"field\": \"CategoryValue\", \"size\": 1000  }  }  }    \t\n");
            sb.Append("      ,\"sort\": "+ sorts.ToArrayPairText() + " \t\n");
            sb.Append("      ,\"query\":{ \"bool\": {  \"must\": { \"multi_match\": { \"query\": \"" + token + "\", \"type\": \"cross_fields\",  \t\n");
            sb.Append("         \"fields\": " + fields.ToArrayText() + " ,   \t\n");
            sb.Append("         \"operator\": \"and\" } }, \"filter\": [ " + filter.ToPairText("match", "DISCOUNT_VALUE") + " \t\n");

            if (filter.ContainsKey("DISCOUNT_VALUE"))
                sb.Append("         { \"range\": { \"DISCOUNT_VALUE\": { \"gte\": " + priceGte + ", \"lte\": " + priceLte + " } } },   \t\n");

            sb.Append("         { \"range\": { \"PRC\": { \"gte\": " + priceGte + ", \"lte\": " + priceLte + " } } }   \t\n");
            sb.Append("     ] } }    \t\n");

            sb.Append("     ,\"highlight\": { \"pre_tags\": [ \"<span class='marked'>\" ], \"post_tags\": [ \"</span>\" ],  \t\n");
            sb.Append("         \"fields\": {   \t\n");
            sb.Append("             \"GOOD_NM\": { \"term_vector\" : \"with_positions_offsets\" },    \t\n");
            sb.Append("             \"Description\": { \"term_vector\" : \"with_positions_offsets\" },  \t\n");
            sb.Append("             \"Specifications\": { \"term_vector\" : \"with_positions_offsets\" }    \t\n");
            sb.Append("   } }   \t\n");
            sb.Append("      ,\"from\": " + from.ToString() + " ,\"size\": " + size.ToString() + " ,\"_source\": " + source.ToArrayText() + " \t\n");

            sb.Append(" } \t\n");

            return sb.ToString();
        }

        public Elastic.Domain.UMall.Product.Mapping.ProductMapping GetTypeMappingJson()
        {
            Elastic.Domain.UMall.Product.Mapping.ProductMapping mapping = new Elastic.Domain.UMall.Product.Mapping.ProductMapping();
            mapping.product = new Elastic.Domain.UMall.Product.Mapping.Product();
            mapping.product._all = new Elastic.Domain.UMall.Product.Mapping._All();
            mapping.product._all.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product._all.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product._all.term_vector = IndexMappingEnum.term_vector_no;
            mapping.product._all.store = IndexMappingEnum.store;

            mapping.product.properties = new Elastic.Domain.UMall.Product.Mapping.Properties();
            mapping.product.properties.GOOD_ID = new Elastic.Domain.UMall.Product.Mapping.GOOD_ID();
            mapping.product.properties.GOOD_ID.type = IndexMappingEnum.type_string;
            mapping.product.properties.GOOD_ID.index = IndexMappingEnum.Not_Analyzed;

            mapping.product.properties.GOOD_NM = new Elastic.Domain.UMall.Product.Mapping.GOOD_NM();
            mapping.product.properties.GOOD_NM.type = IndexMappingEnum.type_string;
            mapping.product.properties.GOOD_NM.store = IndexMappingEnum.store;
            mapping.product.properties.GOOD_NM.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.GOOD_NM.analyzer = IndexMappingEnum.Analyzer_ik_max_word;
            mapping.product.properties.GOOD_NM.search_analyzer = IndexMappingEnum.Analyzer_ik_max_word;
            mapping.product.properties.GOOD_NM.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.GOOD_NM.boost = IndexMappingEnum.boost;

            mapping.product.properties.PRC = new Elastic.Domain.UMall.Product.Mapping.PRC();
            mapping.product.properties.PRC.type = IndexMappingEnum.type_int;

            mapping.product.properties.Specifications = new Elastic.Domain.UMall.Product.Mapping.Specifications();
            mapping.product.properties.Specifications.type = IndexMappingEnum.type_string;
            mapping.product.properties.Specifications.store = IndexMappingEnum.store;
            mapping.product.properties.Specifications.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.Specifications.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Specifications.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Specifications.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.Specifications.boost = IndexMappingEnum.boost;

            mapping.product.properties.Description = new Elastic.Domain.UMall.Product.Mapping.Description();
            mapping.product.properties.Description.type = IndexMappingEnum.type_string;
            mapping.product.properties.Description.store = IndexMappingEnum.store;
            mapping.product.properties.Description.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.Description.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Description.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.Description.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.Description.boost = IndexMappingEnum.boost;

            mapping.product.properties.BUNDLE_CD = new Elastic.Domain.UMall.Product.Mapping.BUNDLE_CD();
            mapping.product.properties.BUNDLE_CD.type = IndexMappingEnum.type_int;

            mapping.product.properties.KEYWORD = new Elastic.Domain.UMall.Product.Mapping.KEYWORD();
            mapping.product.properties.KEYWORD.type = IndexMappingEnum.type_string;
            mapping.product.properties.KEYWORD.store = IndexMappingEnum.store;
            mapping.product.properties.KEYWORD.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.KEYWORD.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.KEYWORD.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.KEYWORD.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.KEYWORD.boost = IndexMappingEnum.boost;

            mapping.product.properties.CategoryValue = new Elastic.Domain.UMall.Product.Mapping.CategoryValue();
            mapping.product.properties.CategoryValue.type = IndexMappingEnum.type_string;

            mapping.product.properties.name = new Elastic.Domain.UMall.Product.Mapping.Name();
            mapping.product.properties.name.type = IndexMappingEnum.type_string;
            mapping.product.properties.name.store = IndexMappingEnum.store;
            mapping.product.properties.name.term_vector = IndexMappingEnum.term_vector;
            mapping.product.properties.name.analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.name.search_analyzer = IndexMappingEnum.Analyzer_ik_smart;
            mapping.product.properties.name.include_in_all = IndexMappingEnum.include_in_all;
            mapping.product.properties.name.boost = IndexMappingEnum.boost;

            mapping.product.properties.deliveryWay = new Elastic.Domain.UMall.Product.Mapping.Deliveryway();
            mapping.product.properties.deliveryWay.type = IndexMappingEnum.type_int;

            mapping.product.properties.deliveryType = new Elastic.Domain.UMall.Product.Mapping.Deliverytype();
            mapping.product.properties.deliveryType.type = IndexMappingEnum.type_int;

            mapping.product.properties.ONLINE = new Elastic.Domain.UMall.Product.Mapping.ONLINE();
            mapping.product.properties.ONLINE.type = IndexMappingEnum.type_int;

            mapping.product.properties.DISCOUNT_VALUE = new Elastic.Domain.UMall.Product.Mapping.DISCOUNT_VALUE();
            mapping.product.properties.DISCOUNT_VALUE.type = IndexMappingEnum.type_int;

            mapping.product.properties.categoryId = new Elastic.Domain.UMall.Product.Mapping.Categoryid();
            mapping.product.properties.categoryId.type = IndexMappingEnum.type_int;

            mapping.product.properties.preferentialPrice = new Elastic.Domain.UMall.Product.Mapping.Preferentialprice();
            mapping.product.properties.preferentialPrice.type = IndexMappingEnum.type_int;

            mapping.product.properties.fugoCompanyId = new Elastic.Domain.UMall.Product.Mapping.Fugocompanyid();
            mapping.product.properties.fugoCompanyId.type = IndexMappingEnum.type_int;

            mapping.product.properties.FugoSaleNo = new Elastic.Domain.UMall.Product.Mapping.Fugosaleno();
            mapping.product.properties.FugoSaleNo.type = IndexMappingEnum.type_int;

            mapping.product.properties.showSaleTipIcon = new Elastic.Domain.UMall.Product.Mapping.Showsaletipicon();
            mapping.product.properties.showSaleTipIcon.type = IndexMappingEnum.type_int;

            mapping.product.properties.MOBI_DISCOUNT_VALUE = new Elastic.Domain.UMall.Product.Mapping.MOBI_DISCOUNT_VALUE();
            mapping.product.properties.MOBI_DISCOUNT_VALUE.type = IndexMappingEnum.type_int;

            mapping.product.properties.IS_SHOW_DISCOUNT = new Elastic.Domain.UMall.Product.Mapping.IS_SHOW_DISCOUNT();
            mapping.product.properties.IS_SHOW_DISCOUNT.type = IndexMappingEnum.type_int;

            mapping.product.properties.Product_Suggest = new Mapping.Product_Suggest();
            mapping.product.properties.Product_Suggest.max_input_length = 50;
            mapping.product.properties.Product_Suggest.payloads = false;
            mapping.product.properties.Product_Suggest.analyzer = "simple";
            mapping.product.properties.Product_Suggest.preserve_position_increments = true;
            mapping.product.properties.Product_Suggest.type = "completion";
            mapping.product.properties.Product_Suggest.preserve_separators = true;

            return mapping;
        }

        public string GetBulkInsertDocumentJson(string indexName, string typeName, string id, string item)
        {
            return "{ \"create\": { \"_index\": \"" + indexName + "\",   \"_type\": \"" + typeName + "\",  \"_id\": \"" + id + "\"  } } \t\n" + item + " \t\n";
        }

        public string GetBulkInsertDocumentText(string indexName, string typeName, string id, Product.Data.Product item)
        {
            return string.Empty;
            // return "{ \"create\": { \"_index\": \"" + indexName + "\",   \"_type\": \"" + typeName + "\",  \"_id\": \"" + id + "\"  } } \t\n" + "{ \"MOBI_DISCOUNT_VALUE\": \"" + item.MOBI_DISCOUNT_VALUE + "\",    \"APP_DISCOUNT_VALUE\": \"" + item.APP_DISCOUNT_VALUE + "\",    \"FugoSaleNo\": \"" + item.FugoSaleNo + "\",    \"AdultProduct\": \"" + item.AdultProduct + "\",    \"TravelProduct\": \"" + item.TravelProduct + "\",    \"CategorySClass\": \"" + item.CategorySClass + "\",    \"CategoryMClass\": \"" + item.CategoryMClass + "\",    \"CategoryLClass\": \"" + item.CategoryLClass + "\",    \"CategoryStore2\": \"" + item.CategoryStore2 + "\",   \"CategoryStore\": \"" + item.CategoryStore + "\",    \"DISCOUNT_VALUE\": \"" + item.DISCOUNT_VALUE + "\",    \"COUPON\": \"" + item.COUPON + "\",    \"HOTSALE\": \"" + item.HOTSALE + "\",    \"HOTVIEW\": \"" + item.HOTVIEW + "\",    \"PRODUCTSOURCE\": \"" + item.PRODUCTSOURCE + "\",    \"ONLINE\": \"" + item.ONLINE + "\",    \"KEYWORD\": \"" + item.KEYWORD + "\",    \"BUNDLE_CD\": \"" + item.BUNDLE_CD + "\",    \"OneDayShip\": \"" + item.OneDayShip + "\",    \"Description\": \"" + item.Description + "\",   \"Specifications\": \"" + item.Specifications + "\",    \"PRC\": \"" + item.PRC + "\",    \"GOOD_NM\": \"" + item.GOOD_NM + "\",    \"GOOD_ID\": \"" + item.GOOD_ID + "\"  } \t\n";

        }

        public string GetSuggestJson(string text)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" { ");
            sb.Append("   \"product_suggest\": { ");
            sb.Append("     \"text\": \"" + text + "\", ");
            sb.Append("     \"completion\": { ");
            sb.Append("       \"field\": \"Product_Suggest\" ");
            sb.Append("     } ");
            sb.Append("   } ");
            sb.Append(" } ");

            return sb.ToString();
        }
    }
}
