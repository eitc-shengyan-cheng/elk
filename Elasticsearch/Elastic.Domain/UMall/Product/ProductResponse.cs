﻿using System;

namespace Elastic.Domain.UMall.Product.Search
{
    public class ProductResponse 
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public _Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Aggregations aggregations { get; set; }
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public object max_score { get; set; }
        public Hit[] hits { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public object _score { get; set; }
        public _Source _source { get; set; }
        public Highlight highlight { get; set; }
        public object[] sort { get; set; }
    }

    public class _Source
    {
        public string Description { get; set; }
        public string GOOD_NM { get; set; }
        public string Specifications { get; set; }
        public int DISCOUNT_VALUE { get; set; }
        public string GOOD_ID { get; set; }
        public double PRC { get; set; }



    }

    public class Highlight
    {
        public string[] GOOD_NM { get; set; }
        public string[] Specifications { get; set; }
        public string[] Description { get; set; }
    }

    public class Aggregations
    {
        public Categorystoreaggs CategoryStoreAGGS { get; set; }
    }

    public class Categorystoreaggs
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public Bucket[] buckets { get; set; }
    }

    public class Bucket
    {
        public string key { get; set; }
        public int doc_count { get; set; }
    }
}