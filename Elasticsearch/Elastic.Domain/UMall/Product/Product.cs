﻿using Elastic.Infra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elastic.Domain.UMall.Product.Data
{
    /// <summary>
    /// 匯入資料的 Schema
    /// CLASS名稱必須和Elasticsearch TYPE相同
    /// </summary>
    public class Product
    {
        /// <summary>
        /// [GOOD_ID][fugoMultiSaleNo],通路碼+銷售編號 
        /// </summary>
        public string GOOD_ID;
        /// <summary>
        /// [GOOD_NM][saleName],銷售名稱
        /// </summary>
        public string GOOD_NM;
        /// <summary>
        /// [PRC][salePrice],東森價/森森價
        /// </summary>
        public double PRC;
        /// <summary>
        /// [Specifications][prdDesPlanner],商品規格
        /// </summary>
        public string Specifications;
        /// <summary>
        /// [Description][SalesSubtitle],副標
        /// </summary>
        public string Description;
        /// <summary>
        /// [OneDayShip][isExpressProduct], 是否為快速到貨：1  是，0  否
        /// </summary>
        public int ONEDAYSHIP;
        /// <summary>
        /// [BUNDLE_CD][id_ProductProperty],商品屬性編號 外鍵(1:一般商品, 2:加價購, 3:競標(KID), 4:任選, 5:預購, 6:贈品, 7:團購, 8:集購, 9:E購物車加價購, 10.低溫冷藏, 11.兌點商品)
        /// </summary>
        public int BUNDLE_CD;
        /// <summary>
        /// [KEYWORD][searchKeyword],ETMall有多銷售編號
        /// </summary>
        public string KEYWORD;
        /// <summary>
        /// [ONLINE][saleStatusId],品號狀態:  4 可銷售/3 銷售不可/2暫下架
        /// </summary>
        public int ONLINE;
        /// <summary>
        /// [COUPON][isfugoCouponUsable],可否使用折價券：1  是，0  否
        /// </summary>
        public int COUPON;
        /// <summary>
        /// [DISCOUNT_VALUE][DISCOUNT_VALUE],優惠價
        /// </summary>
        public int DISCOUNT_VALUE;
        /// <summary>
        /// [FugoSaleNo][fugoSaleNo]
        /// </summary>
        public int FugoSaleNo;
        /// <summary>
        /// [fugoProductID][fugoProductID]
        /// </summary>
        public string fugoProductID;
        /// <summary>
        /// [MOBI_DISCOUNT_VALUE][MOBI_DISCOUNT_VALUE],小網優惠價
        /// </summary>
        public int MOBI_DISCOUNT_VALUE;
        /// <summary>
        /// [deliveryWay], 配送方式 1:庫送 2:指送(廠送) 3:到廠取貨 4.特約
        /// </summary>
        public int deliveryWay;
        /// <summary>
        /// [deliveryType],配送類型. 1:一般 4:超商取貨
        /// </summary>
        public int STORESERVICE;
        /// <summary>
        /// [lastModifiedTime],商品最後修改時間
        /// </summary>
        public string lastModifiedTime;
        /// <summary>
        /// [categoryId],後台分類編號
        /// </summary>
        public string categoryId;
        /// <summary>
        /// [CategoryPath],分類路徑 ETMall 用 - 分隔, Umall 用 ## 分隔
        /// </summary>
        public string CategoryValue;
        /// <summary>
        /// [saleCode],網站編號 ( REMOVE?)
        /// </summary>
        public string saleCode;
        /// <summary>
        /// [name],商品名稱
        /// </summary>
        public string name;
        /// <summary>
        /// [preferentialPrice], (UHoliday)優惠價
        /// </summary>
        public int preferentialPrice;
        /// <summary>
        /// [fugoCompanyId], 公司代碼，EU不同 ( REMOVE )
        /// </summary>
        public int fugoCompanyId;
        /// <summary>
        /// [showSaleTipIcon], 顯示折價小幫手
        /// </summary>
        public int showSaleTipIcon;
        /// <summary>
        /// [IS_SHOW_DISCOUNT]
        /// </summary>
        public int IS_SHOW_DISCOUNT;
        /// <summary>
        /// [last7d], 近7天銷售數量,[REMOVE]
        /// </summary>
        public int last7d;
        //public int hiddenPreferentialPrice;

        public Product_Suggest Product_Suggest;

        public static IEnumerable<Product> Convert(IEnumerable<string> data)
        {
            return data.Select(x => x.Split('\t')).Skip(1).Select(f => new Product
            {
                GOOD_ID = f[0],
                GOOD_NM = f[1],
                CategoryValue = f[2],
                PRC = f[3].ParseDouble(),
                Specifications = f[4],
                ONEDAYSHIP = f[5].ParseBool(),  //isExpressProduct
                KEYWORD = f[6],
                fugoProductID = f[7],
                fugoCompanyId = f[8].ParseInt(),
                COUPON = f[9].ParseBool(),
                DISCOUNT_VALUE = f[10].ParseInt(),
                MOBI_DISCOUNT_VALUE = f[11].ParseInt(),
                saleCode = f[12],
                FugoSaleNo = f[0].ParseInt(),
                name = f[14],
                preferentialPrice = f[15].ParseInt(),
                ONLINE = 1,  // 匯出來源皆為4, 且配合EU格式一致,固定寫為1
                categoryId = f[17],
                BUNDLE_CD = f[18].ParseInt(),
                lastModifiedTime = f[19],
                showSaleTipIcon = f[20].ParseInt(),
                Description = f[21],
                deliveryWay = f[22].ParseInt(),
                STORESERVICE = f[23].Contains("4") ? 1 : 0,  //deliveryType 配送類型. 1:一般 4:超商取貨
                IS_SHOW_DISCOUNT = f[24].ParseInt(),
                last7d = f[25].ParseInt(),
                Product_Suggest = new Product_Suggest()
                {
                    input = f[6].Split(',')
                }
            });
        }
    }

    public class Product_Suggest
    {
        public string[] input { get; set; }
    }
}
