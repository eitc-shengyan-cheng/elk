﻿using System;

namespace Elastic.Domain.UMall.Product.Search
{
    public class ProductSingleResponse
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public bool found { get; set; }
        public ProductSingleResponseSource _source { get; set; }
    }

    public class ProductSingleResponseSource
    {
        public string Description { get; set; }
        public string GOOD_NM { get; set; }
        public string Specifications { get; set; }
        public int DISCOUNT_VALUE { get; set; }
        public string GOOD_ID { get; set; }
        public double PRC { get; set; } 
        public int ONEDAYSHIP { get; set; } 
        public int BUNDLE_CD { get; set; }
        public string KEYWORD { get; set; }
        public int ONLINE { get; set; } 
        public int COUPON { get; set; }
        public int FugoSaleNo { get; set; }
        public string fugoProductID { get; set; }
        public int MOBI_DISCOUNT_VALUE { get; set; } 
        public int deliveryWay { get; set; } 
        public int STORESERVICE { get; set; }
        public string lastModifiedTime { get; set; } 
        public string categoryId { get; set; } 
        public string CategoryValue { get; set; } 
        public string saleCode { get; set; } 
        public string name { get; set; }
        public int preferentialPrice { get; set; }
        public int fugoCompanyId { get; set; }
        public int showSaleTipIcon { get; set; } 
        public int IS_SHOW_DISCOUNT { get; set; }
        public int last7d { get; set; }
    } 
}