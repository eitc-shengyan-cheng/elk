﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Elastic.Domain.UMall.Product
{
    public static class ProductFilter
    {
        /// <summary>
        /// 取得搜尋的欄位和權重
        /// </summary>
        /// <param name="tokens"></param>
        /// <returns></returns>
        public static List<string> GetFields(string tokens)
        {
            if (tokens == null || tokens == string.Empty)
            {
                return new List<string>() { "GOOD_NM^100", "Specifications^2", "Description", "keyword^2" };
            }

            // 就[IPHONE 7]這類有單一數字的查詢,給予不同查詢欄位和權重
            // 是否為多個字詞,是否為數字,是否長度為小於2的字元
            if (tokens.Contains(string.Empty))
            {
                var token = tokens.Split(' ');
                var reg = new Regex(@"^[0-9]");

                for (int i = 0; i < token.Length; i++)
                {
                    if (reg.Matches(token[i]).Count > 0)
                    {
                        if (token[i].Length <= 2)
                            return new List<string>() { "GOOD_NM^100" };
                    }
                }
            }

            return new List<string>() {
                "GOOD_NM^100", "Specifications^2", "Description", "keyword^2"
            };
        }
    }
}
