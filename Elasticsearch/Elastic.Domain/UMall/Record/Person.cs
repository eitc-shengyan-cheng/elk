﻿using System;

namespace Elastic.Domain.UMall.Record.Data
{
    public class Person
    {
        public int Times { get; set; }
        public string CUST_ID { get; set; }
        public string Word { get; set; }
        public Word_Suggest Word_Suggest { get; set; }
    }

    public class Word_Suggest
    {
        public string[] input { get; set; }
    }
}