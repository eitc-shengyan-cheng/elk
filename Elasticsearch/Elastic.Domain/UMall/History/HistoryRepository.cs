﻿using System.Text;
using System;

namespace Elastic.Domain.UMall.History
{
    public class HistoryRepository
    {
        public Data.History GetInsertEntity(string account, string word, dynamic json, DateTime date, int count = 0)
        {
            Data.History h = new Data.History()
            {
                Account = account,
                Count = count,
                Date = string.Format("{0:yyyy-MM-dd}", date),
                Json = json,
                Word = new string[] { word },
                Word_Suggest = new Data.Word_Suggest()
                {
                    input = new string[] { word }
                }
            };

            return h;
        }

        public Elastic.Domain.UMall.History.Mapping.HistoryMapping GetTypeMappingJson()
        {
            Elastic.Domain.UMall.History.Mapping.HistoryMapping mapping = new UMall.History.Mapping.HistoryMapping();
            mapping.history = new Mapping.History();
            mapping.history._all = new Mapping._All();
            mapping.history._all.enabled = false;

            mapping.history.properties = new Elastic.Domain.UMall.History.Mapping.Properties();
            mapping.history.properties.Account = new Elastic.Domain.UMall.History.Mapping.Account();
            mapping.history.properties.Account.index = IndexMappingEnum.Not_Analyzed;
            mapping.history.properties.Account.type = "string";

            mapping.history.properties.Date = new Elastic.Domain.UMall.History.Mapping.Date();
            mapping.history.properties.Date.format = "yyyy-MM-dd";
            mapping.history.properties.Date.type = "date";

            mapping.history.properties.Json = new Elastic.Domain.UMall.History.Mapping.Json();
            mapping.history.properties.Json.enabled = false;
            mapping.history.properties.Json.type = "object";

            mapping.history.properties.Word = new Elastic.Domain.UMall.History.Mapping.Word();
            mapping.history.properties.Word.index = IndexMappingEnum.Not_Analyzed;
            mapping.history.properties.Word.type = "string";

            mapping.history.properties.Count = new Elastic.Domain.UMall.History.Mapping.Count();
            mapping.history.properties.Count.index = IndexMappingEnum.Not_Analyzed;
            mapping.history.properties.Count.type = "integer";

            mapping.history.properties.Word_Suggest = new Elastic.Domain.UMall.History.Mapping.Word_Suggest();
            mapping.history.properties.Word_Suggest.max_input_length = 50;
            mapping.history.properties.Word_Suggest.payloads = false;
            mapping.history.properties.Word_Suggest.analyzer = "simple";
            mapping.history.properties.Word_Suggest.preserve_position_increments = true;
            mapping.history.properties.Word_Suggest.type = "completion";
            mapping.history.properties.Word_Suggest.preserve_separators = true;

            return mapping;
        }

        public string GetRankJson(DateTime begin, DateTime end, int size)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("  {  ");
            sb.Append("     \"aggs\": { ");
            sb.Append("         \"AggsWord\": {    ");
            sb.Append("             \"terms\": {    ");
            sb.Append("                 \"field\": \"Word\",   ");
            sb.Append("                 \"size\": 10    ");
            sb.Append("             }   ");
            sb.Append("         }   ");
            sb.Append("     },  ");
            sb.Append("     \"sort\": [ ");
            sb.Append("         { \"Account\": \"desc\" }   ");
            sb.Append("     ],  ");
            sb.Append("   \"query\": {  ");
            sb.Append("     \"bool\": { ");
            sb.Append("       \"must\": {   ");
            sb.Append("         \"query\": {    ");
            sb.Append("           \"match_all\": {} ");
            sb.Append("         }   ");
            sb.Append("       },    ");
            sb.Append("       \"filter\": [ ");
            sb.Append("         {   ");
            sb.Append("           \"range\": {  ");
            sb.Append("             \"Date\": { ");
            sb.Append("               \"gte\": \"" + string.Format("{0:yyyy-MM-dd}", begin) + "\", ");
            sb.Append("               \"lte\": \"" + string.Format("{0:yyyy-MM-dd}", end) + "\"  ");
            sb.Append("             }   ");
            sb.Append("           } ");
            sb.Append("          }  ");
            sb.Append("       ] ");
            sb.Append("     }   ");
            sb.Append("   },    ");
            sb.Append("   \"from\": 0,  ");
            sb.Append("   \"size\": " + size + "   ");
            sb.Append(" } 	");

            return sb.ToString();
        }

        public string GetUserJson(string user, DateTime begin, DateTime end, int size)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" { ");
            sb.Append("     \"aggs\": { ");
            sb.Append("         \"AggsWord\": { ");
            sb.Append("             \"terms\": { ");
            sb.Append("                 \"field\": \"Word\", ");
            sb.Append("                 \"size\": " + size + "  ");
            sb.Append("                         } ");
            sb.Append("                     } ");
            sb.Append("                 }, ");
            sb.Append("     \"sort\": [ ");
            sb.Append("         { \"Account\": \"desc\" } ");
            sb.Append("     ], ");
            sb.Append("     \"query\": { ");
            sb.Append("         \"bool\": { ");
            sb.Append("             \"must\": { ");
            sb.Append("                 \"multi_match\": { ");
            sb.Append("                     \"query\": \"" + user + "\", ");
            sb.Append("                     \"type\": \"cross_fields\", ");
            sb.Append("                     \"fields\": [ \"Account\" ], ");
            sb.Append("                     \"operator\": \"and\" ");
            sb.Append("                 } ");
            sb.Append("           }, ");
            sb.Append("           \"filter\": [ ");
            sb.Append("             { ");
            sb.Append("               \"range\": { ");
            sb.Append("                 \"Date\": { ");
            sb.Append("                   \"gte\": \"" + string.Format("{0:yyyy-MM-dd}", begin) + "\", ");
            sb.Append("                   \"lte\": \"" + string.Format("{0:yyyy-MM-dd}", end) + "\" ");
            sb.Append("                 } ");
            sb.Append("               } ");
            sb.Append("             } ");
            sb.Append("           ] ");
            sb.Append("         } ");
            sb.Append("       }, ");
            sb.Append("   \"from\": 0, ");
            sb.Append("   \"size\": 0 ");
            sb.Append(" } ");

            return sb.ToString();
        }

        public string GetSuggestJson(string text)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" { ");
            sb.Append("   \"product_suggest\": { ");
            sb.Append("     \"text\": \"" + text + "\", ");
            sb.Append("     \"completion\": { ");
            sb.Append("       \"field\": \"Word_Suggest\" ");
            sb.Append("     } ");
            sb.Append("   } ");
            sb.Append(" } ");

            return sb.ToString();
        }
    }
}
