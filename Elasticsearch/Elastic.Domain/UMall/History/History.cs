﻿using System;

namespace Elastic.Domain.UMall.History.Data
{
    public class History
    {
        public string Account { get; set; }
        public string Date { get; set; }
        public int Count { get; set; }
        public dynamic Json { get; set; }
        public string[] Word { get; set; }
        public Word_Suggest Word_Suggest { get; set; }
    }

    public class Word_Suggest
    {
        public string[] input { get; set; }
    }
}