﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elastic.Domain.UMall.History.Mapping
{

    public class HistoryMapping
    {
        public History history { get; set; }
    }

    public class History
    {
        public _All _all { get; set; }
        public Properties properties { get; set; }
    }

    public class _All
    {
        public bool enabled { get; set; }
    }

    public class Properties
    {
        public Account Account { get; set; }
        public Word Word { get; set; }
        public Count Count { get; set; }
        public Json Json { get; set; }
        public Word_Suggest Word_Suggest { get; set; }
        public Date Date { get; set; }
    }

    public class Account
    {
        public string index { get; set; }
        public string type { get; set; }
    }

    public class Word
    {
        public string index { get; set; }
        public string type { get; set; }
    }

    public class Count
    {
        public string index { get; set; }
        public string type { get; set; }
    }

    public class Json
    {
        public string type { get; set; }
        public bool enabled { get; set; }
    }

    public class Word_Suggest
    {
        public int max_input_length { get; set; }
        public bool payloads { get; set; }
        public string analyzer { get; set; }
        public bool preserve_position_increments { get; set; }
        public string type { get; set; }
        public bool preserve_separators { get; set; }
    }

    public class Date
    {
        public string format { get; set; }
        public string type { get; set; }
    }

}
