﻿using Elastic.Application;
using Elastic.Domain;
using Elastic.Domain.ETMall.Product;
using Elastic.Domain.ETMall.Product.Data;
using Elastic.Infra;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Elastic.Task.DataProvider
{
    public class ETMallExecutor : Executor<Domain.ETMall.Product.Data.Product>, IExecutor
    {
        protected Elastic.Domain.ETMall.Product.ProductRepository repository = new ProductRepository();

        /// <summary>
        /// 存取本機XML資料
        /// </summary>
        /// <returns></returns>
        public IExecutor ReadLocalXml()
        {
            var path = DataSourceSetting.LocalDataFile;
            XElement xml = XElement.Load(path);

            IEnumerable<XElement> list =
                from el in xml.Elements("item")
                select el;

            foreach (XElement el in list)
            {
                /*
                Item i = new Item();
                i.AdultProduct = el.Element("AdultProduct").Value;
                i.allcategory = el.Element("allcategory").Value;
                //i.Bundle_cd = el.Element("Bundle_cd").Value;
                i.categoryid = el.Element("categoryid").Value;
                i.categoryname = el.Element("categoryname").Value;
                //i.channel = el.Element("channel").Value;
                //i.create = el.Element("create").Value;
                //i.Deeplink = el.Element("Deeplink").Value;
                //i.desc = el.Element("desc").Value;
                i.enddatetime = el.Element("enddatetime").Value;
                //i.Hiwey = el.Element("Hiwey").Value;
                i.img_link = el.Element("img_link").Value;
               // i.online = el.Element("online").Value;
                i.pk = el.Element("pk").Value;
                i.startdatetime = el.Element("startdatetime").Value;
                i.storeid = el.Element("storeid").Value;
                i.storename = el.Element("storename").Value;
                i.tag = el.Element("tag").Value;
                i.title = el.Element("title").Value;
                i.update = el.Element("update").Value;
                //i.url_disp = el.Element("url_disp").Value;
                //i.url_link = el.Element("url_link").Value;
                //i.va_a = el.Element("va_a").Value;
                //i.va_b = el.Element("va_b").Value;
                //i.va_c = el.Element("va_c").Value;
                //i.va_d = el.Element("va_d").Value;
                this.data.Add(i);
                */
            }

            Console.WriteLine(this.data.Count);

            return this;
        }

        /// <summary>
        /// 讀取本機CSV資料檔
        /// </summary>
        /// <returns></returns>
        public IExecutor ReadLocalCsv()
        {
 
                var path = DataSourceSetting.LocalDataFile;
                var stuff = Product.Convert(File.ReadLines(path));
                this.data.AddRange(stuff);
            /*
            catch (Exception ex)
            {
                this.HasError = true;
                this.exception.Add(new MethodResult<string>()
                {
                    Command = string.Empty,
                    Parameter = string.Empty,
                    Name = "ReadLocalCsv",
                    Data = ex.InnerException.Source,
                    Message = ex.InnerException.Message,
                    HasError = true
                });
            }
            */
            return this;
        }

        /// <summary>
        /// 新增Document至ES的TYPE
        /// TYPE預設名稱為 [{typeName}.YYYYMMDD],TYPE依當天日期來建立, INDEX需預先建置
        /// </summary>
        /// <returns></returns>
        public IExecutor InsertDocument()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var client = new RestClient();
            client.BaseUrl = new Uri(ElasticSetting.Host);

            IRestResponse response = null;
            int total = this.data.Count;

            for (int i = 0; i <= total - 1; i++)
            {
                RestRequest request = new RestRequest(string.Format("{0}/{1}/{2}", IndexProductSetting.GetIndexName(), IndexProductSetting.GetTypeName(), data[i].GOOD_ID), Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.AddBody(data[i]);
                response = client.Execute(request);

                Console.WriteLine(data[i].GOOD_ID);
                Console.WriteLine(data[i].GOOD_NM);

                this.exception.Add(new MethodResult<string>()
                {
                    Command = data[i].ToString(),
                    Parameter = request.Resource,
                    Name = "BulkInsertDocument",
                    Data = response.Content.ToString(),
                    Message = response.StatusCode.ToString(),
                });
            }

            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            return this;
        }

        /// <summary>
        /// 批次新增Document至ES的TYPE
        /// TYPE預設名稱為 [{typeName}.YYYYMMDD],TYPE依當天日期來建立, INDEX需預先建置
        /// </summary>
        /// <returns></returns>
        public IExecutor BulkInsertDocument()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var client = new RestClient();
            client.BaseUrl = new Uri(ElasticSetting.Host);

            int count = 0;
            int total = this.data.Count;
            int total_1 = total - 1;

            string indexName = IndexProductSetting.GetIndexName();
            string typeName = IndexProductSetting.GetTypeName();
            string url = string.Format("{0}/{1}/_bulk", IndexProductSetting.GetIndexName(), IndexProductSetting.GetTypeName());

            StringBuilder bulk = new StringBuilder();

            for (int i = 0; i <= total_1; i++)
            {
                count++;

                var json = SerializeUtils.Serialize<Product>(data[i]);

                bulk.Append(this.repository.GetBulkInsertDocumentJson(indexName, typeName , data[i].GOOD_ID, json));

                if (count == DataSourceSetting.BulkCount || (total == (i + 1)))
                {
                    RestRequest request = new RestRequest(url, Method.POST);
                    request.AddParameter("text/json", bulk.ToString(), ParameterType.RequestBody);

                    var response = client.Execute(request);

                    Console.WriteLine(string.Format("BulkInsertDocument:{0}:{1}", data[i].GOOD_ID, data[i].GOOD_NM) + Environment.NewLine);

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        this.exception.Add(new MethodResult<string>()
                        {
                            Command = bulk.ToString(),
                            Parameter = request.Resource,
                            Name = string.Format("BulkInsertDocument:{0}", data[i].GOOD_ID),
                            Data = response.Content.ToString(),
                            Message = response.StatusCode.ToString(),
                            HasError = true
                        });
                    }

                    count = 0;
                    bulk.Clear();
                }
            }

            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            return this;
        }

        public IExecutor SetIndex()
        {
            base.SetIndex(IndexProductSetting.GetIndexName());
            return this;
        }

        public IExecutor RemoveIndex()
        {
            base.RemoveIndex(IndexProductSetting.GetIndexRemoveName());
            return this;
        }

        public IExecutor SetTypeMapping()
        {
            base.SetTypeMapping(IndexProductSetting.GetIndexName(), IndexProductSetting.GetTypeName(), this.repository.GetTypeMappingJson());
            return this;
        }

        public IExecutor SetIndexAliase()
        {
            base.SetIndexAliase(IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetLastIndexName(), IndexProductSetting.GetIndexName());
            return this;
        }

        public IExecutor RemoveIndexAliase()
        {
            if (HasError == false)
            {
                base.RemoveIndexAliase(IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetLastIndexName());
            }
            return this;
        }

        public IExecutor CreateIndexAliase()
        {
            if (HasError == false)
            {
                base.CreateIndexAliase(IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetIndexName());
            }
            return this;
        }

        public IExecutor SetTypeShardSetting(int seconds, string durability, int ops)
        {
            base.SetTypeShardSetting(IndexProductSetting.GetIndexName(), seconds, durability, ops);
            return this;
        }

        public IExecutor OptimizeIndex()
        {
            base.SetOptimize(IndexProductSetting.GetIndexName());
            return this;
        }

        public new IExecutor Output()
        {
            base.Output();
            return this;
        }
    }
}
