﻿using System;
using System.Configuration;

namespace Elastic.Task.DataProvider
{
    public static class DataSourceSetting
    {
        /// <summary>
        /// 本機資料檔案路徑
        /// </summary>
        public static string LocalDataFile = ConfigurationManager.AppSettings["ELK_LocalDataFile"] + ConfigurationManager.AppSettings["ELK_Site"] + ".txt";

        /// <summary>
        /// 遠端資料來源HOST
        /// (未使用)
        /// </summary>
        // public static string RemoteDataHost = ConfigurationManager.AppSettings["ELK_RemoteDataHost"];

        /// <summary>
        /// 遠端資料來源URL
        /// (未使用)
        /// </summary>
        // public static string RemoteDataUrl = ConfigurationManager.AppSettings["ELK_RemoteDataUrl"];

        /// <summary>
        /// 批次匯入時,累積N筆後送出
        /// </summary>
        public static int BulkCount = int.Parse(ConfigurationManager.AppSettings["ELK_BulkCount"]);
 
    }
}
