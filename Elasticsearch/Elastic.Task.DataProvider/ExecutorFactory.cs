﻿using System;

namespace Elastic.Task.DataProvider
{
    internal class ExecutorFactory
    {
        internal IExecutor GetExecutor(string b2cName)
        {
            switch (b2cName.ToLower())
            {
                case "etmall":
                    return new ETMallExecutor();
                case "umall":
                    return new UMallExecutor();
                default:
                    return new ETMallExecutor();
            }
        }
    }
}