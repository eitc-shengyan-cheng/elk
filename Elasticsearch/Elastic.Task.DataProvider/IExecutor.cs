﻿namespace Elastic.Task.DataProvider
{
    public interface IExecutor
    {
        IExecutor BulkInsertDocument();
        IExecutor CreateIndexAliase();
        IExecutor InsertDocument();
        IExecutor Output();
        IExecutor ReadLocalCsv();
        IExecutor ReadLocalXml();
        IExecutor RemoveIndex();
        IExecutor RemoveIndexAliase();
        IExecutor SetIndex();
        IExecutor SetIndexAliase();
        IExecutor SetTypeMapping();
        IExecutor SetTypeShardSetting(int seconds, string durability, int ops);
        IExecutor OptimizeIndex();
    }
}