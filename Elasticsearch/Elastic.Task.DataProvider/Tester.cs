﻿using Elastic.Application;
using Elastic.Application.DTO;
using Elastic.Infra;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Elastic.Task.DataProvider
{
    public class Tester
    {
        public static RestClient client = new RestClient();
        public static RestRequest requestProduct = new RestRequest("etmall/product");
        public static RestRequest requestHistory = new RestRequest("etmall/history");

        public Tester()
        {

        }

        public static void LoadSearch(int total)
        {
            ProductSearchRequest request = new ProductSearchRequest()
            {
                Tokens = DateTime.Now.Second.ToString(),
                Account = "004"
            };

            request.Filter = new Dictionary<string, string>();
            //request.Filter.Add("ONLINE", "1");
            //request.Filter.Add("CategoryStore", "視聽家電");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i <= total; i++)
            {
                Search(request);
            }

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("LoadSearch:{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            Console.WriteLine(elapsedTime);
        }

        public static async Task<Elastic.Domain.ETMall.Product.Search.ProductResponse> LoadSearchAsync(int total)
        {
            ProductSearchRequest request = new ProductSearchRequest()
            {
                Tokens = "3M",
                Account = "004"
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");
            //request.Filter.Add("CategoryStore", "視聽家電");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 0;
            request.PriceRangeLte = 50000;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i <= 5; i++)
            {
                await SearchAsync(request);
            }

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            Console.WriteLine(elapsedTime);
            return await SearchAsync(request);
        }

        public static void Run(Action<RestClient, int> action)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);

            Stopwatch time = new Stopwatch();
            time.Start();

            action(client, 1);

            time.Stop();
            Console.WriteLine("time:" + time.Elapsed.ToString());
        }

        public static void RunAsync(Action<RestClient, int> action)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);

            Stopwatch time = new Stopwatch();
            time.Start();

            for (int i = 0; i <= 50000; i++)
            {
                Console.WriteLine("index:" + i.ToString());
                action(client, i);
            }
            time.Stop();
            Console.WriteLine("time:" + time.ElapsedMilliseconds.ToString());

            var temp = string.Empty;
        }

        public static void Run(Action action, int count)
        {
            Stopwatch time = new Stopwatch();
            time.Start();

            for (int i = 0; i <= count; i++)
            {
                action();
            }
             
            TimeSpan ts = time.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
        }


        public static void TestCase()
        {
            ProductSearchRequest request = new ProductSearchRequest()
            {
                Tokens = DateTime.Now.ToLongTimeString()
            };

            request.Filter = new Dictionary<string, string>();
            request.Filter.Add("ONLINE", "1");

            request.Sort = new Dictionary<string, string>();
            request.Sort.Add("GOOD_NM", "desc");
            request.Sort.Add("DISCOUNT_VALUE", "desc");

            request.PriceRangeGte = 1;
            request.PriceRangeLte = 50000;

            RestSharpUtils.PostJsonBody("etmall/product/search", request, "product.loadtesting");
        }

        public static dynamic Search(ProductSearchRequest command)
        {
            if (ElasticSetting.GetSite() == "etmall")
            {
                Application.ETMall.ProductControllerService app = new Application.ETMall.ProductControllerService();
                return app.SearchFor(command);
            }
            else
            {
                Application.UMall.ProductControllerService app = new Application.UMall.ProductControllerService();
                return app.SearchFor(command);
            }

        }

        public static async Task<dynamic> SearchAsync(ProductSearchRequest command)
        {
            if (ElasticSetting.GetSite() == "etmall")
            {
                Application.ETMall.ProductControllerService app = new Application.ETMall.ProductControllerService();
                return await app.SearchForAsync(command);
            }
            else
            {
                Application.UMall.ProductControllerService app = new Application.UMall.ProductControllerService();
                return await app.SearchForAsync(command);
            }
        }
    }
}
