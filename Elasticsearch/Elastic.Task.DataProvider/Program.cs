﻿using Elastic.Application;
using Elastic.Application.DTO;
using Elastic.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Elastic.Task.DataProvider
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                ExecutorFactory factory = new ExecutorFactory();
                var executor = factory.GetExecutor(ElasticSetting.GetSite());

                executor
                    .ReadLocalCsv()
                    .SetIndex()
                    .SetTypeMapping()
                    .SetTypeShardSetting(-1, "async", 100000)
                    .BulkInsertDocument()
                    .SetTypeShardSetting(1, "request", 5000)
                    .CreateIndexAliase()
                    .RemoveIndexAliase()
                    .RemoveIndex()
                    .OptimizeIndex()
                    .Output();
            }
            else
            {
                Tester.LoadSearch(int.Parse(args[0]));
            }

        }
    }
}