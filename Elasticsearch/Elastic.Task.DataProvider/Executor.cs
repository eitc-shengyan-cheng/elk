﻿using Elastic.Application;
using Elastic.Domain;
using Elastic.Infra;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Elastic.Task.DataProvider
{
    public class Executor<D>  
    {
        protected List<MethodResult<string>> exception = new List<MethodResult<string>>();
        protected List<D> data = new List<D>();
        protected bool HasError = false;

        /// <summary>
        /// 建立INDEX
        /// </summary>
        public void SetIndex(string indexName)
        {
            var dsl = new IndexTypeRepository().GetIndexSettingJson(indexName);
            var res = string.Format("{0}", indexName);
            this.exception.Add(RestSharpUtils.PostJsonParameter(res, dsl, "SetIndex"));
        }

        /// <summary>
        /// 移除INDEX
        /// </summary>
        /// <returns></returns>
        public void RemoveIndex(string indexName)
        {
            var res = string.Format("{0}", indexName);
            this.exception.Add(RestSharpUtils.DeleteJsonParameter(res, null , "RemoveIndex"));
        }

        /// <summary>
        /// 建立TYPE的MAPPING
        /// </summary>
        /// <param name="indexName"></param>
        /// <param name="typeName"></param>
        /// <param name="isPeriod"></param>
        /// <returns></returns>
        public void SetTypeMapping(string indexName, string typeName, object dsl)
        {
            var res = string.Format("{0}/{1}/_mapping", indexName, typeName);
            this.exception.Add(RestSharpUtils.PostJsonBody(res, dsl, "SetTypeMapping"));
        }

        /// <summary>
        /// 設定別名 ( 移除昨天的INDEX別名, 改指到今天的INDEX )
        /// </summary>
        /// <param name="aliase"></param>
        /// <returns></returns>
        public void SetIndexAliase(string aliase, string indexName, string lastIndexName)
        {
            var dsl = new IndexTypeRepository().GetAliasesSettingJson(aliase, lastIndexName, indexName);
            var res = "_aliases";
            this.exception.Add(RestSharpUtils.PostJsonParameter(res, dsl, "SetIndexAliase"));
        }

        /// <summary>
        /// 移除INDEX別名 ( 通常是移除昨天的INDEX別名 )
        /// </summary>
        /// <param name="aliase"></param>
        /// <param name="indexName"></param>
        /// <returns></returns>
        public void RemoveIndexAliase(string aliase, string indexName)
        {
            var dsl = new IndexTypeRepository().GetRemoveAliasesSettingJson(aliase, indexName);
            var res = "_aliases";
            this.exception.Add(RestSharpUtils.PostJsonParameter(res, dsl, "RemoveIndexAliase"));
        }

        /// <summary>
        /// 建立INDEX別名 ( 通常是建立當天的INDEX別名 )
        /// </summary>
        /// <param name="aliase"></param>
        /// <param name="indexName"></param>
        /// <returns></returns>
        public void CreateIndexAliase(string aliase, string indexName)
        {
            var dsl = new IndexTypeRepository().GetCreateAliasesSettingJson(aliase, indexName);
            var res = "_aliases";
            this.exception.Add(RestSharpUtils.PostJsonParameter(res, dsl, "CreateIndexAliase"));
        }

        /// <summary>
        /// 更改refresh_interval,在匯入資料時放寬,完成時恢復設定,以提高匯入速度
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public void SetTypeShardSetting(string indexName, int seconds, string durability, int ops)
        {
            var dsl = new IndexTypeRepository().GetTypeRefreshSetting(seconds.ToString(), durability, ops.ToString());
            var res = string.Format("{0}/_settings", indexName);
            this.exception.Add(RestSharpUtils.PutJsonParameter(res, dsl, "SetTypeShardSetting"));            
        }

        /// <summary>
        /// 合併segments
        /// </summary>
        /// <param name="indexName"></param>
        public void SetOptimize(string indexName)
        {
            var res = string.Format("{0}/_optimize?max_num_segments=1", indexName);
            this.exception.Add(RestSharpUtils.GetJsonParameter(res,  "SetOptimize"));
        }

        /// <summary>
        /// 顯示所有執行結果
        /// </summary>
        /// <param name="exceptions"></param>
        public void Output()
        {
            foreach( var ent in this.exception)
            {
                Console.WriteLine(string.Format("{0}:{1}:{2}" + Environment.NewLine, ent.Name + Environment.NewLine, ent.Message + Environment.NewLine, ent.Data + Environment.NewLine));
            }
        }
    }
}


