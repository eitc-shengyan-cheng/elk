﻿using Elastic.Application;
using Elastic.Domain;
using Elastic.Domain.UMall;
using Elastic.Domain.UMall.Product;
using Elastic.Domain.UMall.Product.Data;
using Elastic.Infra;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Elastic.Task.DataProvider
{
    public class UMallExecutor : Executor<Domain.UMall.Product.Data.Product>, IExecutor
    {
        protected Elastic.Domain.UMall.Product.ProductRepository repository = new ProductRepository();
        /// <summary>
        /// 存取遠端XML資料
        /// </summary>
        /// <returns></returns>
        public IExecutor ReadRemoteXml()
        {
            return this;
        }

        /// <summary>
        /// 存取遠端XML資料
        /// </summary>
        /// <param name="host"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public IExecutor ReadRemoteXml(string host, string url)
        {
            return this;
        }

        /// <summary>
        /// 存取本機XML資料
        /// </summary>
        /// <returns></returns>
        public IExecutor ReadLocalXml()
        {
            return this;
        }

        /// <summary>
        /// 讀取本機CSV資料檔
        /// </summary>
        /// <returns></returns>
        public IExecutor ReadLocalCsv()
        {

                var path = DataSourceSetting.LocalDataFile;
                var stuff = Product.Convert(File.ReadLines(path));
                this.data.AddRange(stuff);
       
            /*
            catch (Exception ex)
            {
                this.HasError = true;
                this.exception.Add(new MethodResult<string>()
                {
                    Command = string.Empty,
                    Parameter = string.Empty,
                    Name = "ReadLocalCsv",
                    Data = ex.StackTrace,
                    Message = ex.Message,
                    HasError = true
                });
            }
            */
            return this;
        }

        /// <summary>
        /// 新增Document至ES的TYPE
        /// TYPE預設名稱為 [{typeName}.YYYYMMDD],TYPE依當天日期來建立, INDEX需預先建置
        /// </summary>
        /// <returns></returns>
        public IExecutor InsertDocument()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var client = new RestClient();
            client.BaseUrl = new Uri(ElasticSetting.Host);

            int count = 0;
            int total = this.data.Count;
            int total_1 = total - 1;

            string indexName = IndexProductSetting.GetIndexName();
            string typeName = IndexProductSetting.GetTypeName();
            string url = string.Format("{0}/{1}/_bulk", IndexProductSetting.GetIndexName(), IndexProductSetting.GetTypeName());

            StringBuilder bulk = new StringBuilder();

            for (int i = 0; i <= total_1; i++)
            {
                count++;

                var json = SerializeUtils.Serialize<Product>(data[i]);

                bulk.Append(this.repository.GetBulkInsertDocumentJson(indexName, typeName, data[i].GOOD_ID.ToString(), json));

                if (count == DataSourceSetting.BulkCount || (total == (i + 1)))
                {
                    RestRequest request = new RestRequest(url, Method.POST);
                    request.AddParameter("text/json", bulk.ToString(), ParameterType.RequestBody);

                    var response = client.Execute(request);

                    Console.WriteLine(string.Format("BulkInsertDocument:{0}:{1}", data[i].GOOD_ID, data[i].GOOD_NM) + Environment.NewLine);

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        this.exception.Add(new MethodResult<string>()
                        {
                            Command = bulk.ToString(),
                            Parameter = request.Resource,
                            Name = string.Format("BulkInsertDocument:{0}", data[i].GOOD_ID),
                            Data = response.Content.ToString(),
                            Message = response.StatusCode.ToString(),
                            HasError = true
                        });
                    }

                    count = 0;
                    bulk.Clear();
                }
            }

            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            return this;
        }

        /// <summary>
        /// 批次新增Document至ES的TYPE
        /// TYPE預設名稱為 [{typeName}.YYYYMMDD],TYPE依當天日期來建立, INDEX需預先建置
        /// </summary>
        /// <returns></returns>
        public IExecutor BulkInsertDocument()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var client = new RestClient();
            client.BaseUrl = new Uri(ElasticSetting.Host);

            int count = 0;
            int total = this.data.Count;
            int total_1 = total - 1;

            string indexName = IndexProductSetting.GetIndexName();
            string typeName = IndexProductSetting.GetTypeName();
            string url = string.Format("{0}/{1}/_bulk", IndexProductSetting.GetIndexName(), IndexProductSetting.GetTypeName());

            StringBuilder bulk = new StringBuilder();

            for (int i = 0; i <= total_1; i++)
            {
                count++;

                var json = SerializeUtils.Serialize<Product>(data[i]);

                bulk.Append(this.repository.GetBulkInsertDocumentJson(indexName, typeName, data[i].FugoSaleNo.ToString(), json));

                if (count == DataSourceSetting.BulkCount || (total == (i + 1)))
                {
                    RestRequest request = new RestRequest(url, Method.POST);
                    request.AddParameter("text/json", bulk.ToString(), ParameterType.RequestBody);

                    var response = client.Execute(request);

                    Console.WriteLine(string.Format("BulkInsertDocument:{0}:{1}", data[i].FugoSaleNo, data[i].GOOD_NM) + Environment.NewLine);

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        this.exception.Add(new MethodResult<string>()
                        {
                            Command = bulk.ToString(),
                            Parameter = request.Resource,
                            Name = string.Format("BulkInsertDocument:{0}", data[i].FugoSaleNo),
                            Data = response.Content.ToString(),
                            Message = response.StatusCode.ToString(),
                            HasError = true
                        });
                    }

                    count = 0;
                    bulk.Clear();
                }
            }

            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            return this;
        }

        public IExecutor SetIndex()
        { 
            base.SetIndex(IndexProductSetting.GetIndexName());
            return this;
        }

        public IExecutor RemoveIndex()
        {
            base.RemoveIndex(IndexProductSetting.GetIndexRemoveName());
            return this;
        }

        public IExecutor SetTypeMapping()
        {
            base.SetTypeMapping(IndexProductSetting.GetIndexName(), IndexProductSetting.GetTypeName(), this.repository.GetTypeMappingJson());
            return this;
        }

        public IExecutor SetIndexAliase()
        {
            base.SetIndexAliase(IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetLastIndexName(), IndexProductSetting.GetIndexName());
            return this;
        }

        public IExecutor RemoveIndexAliase()
        {
            if (HasError == false)
            {
                base.RemoveIndexAliase(IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetLastIndexName());
            }
            return this;
        }

        public IExecutor CreateIndexAliase()
        {
            if (HasError == false)
            {
                base.CreateIndexAliase(IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetIndexName());
            }
            return this;
        }

        public IExecutor SetTypeShardSetting(int seconds, string durability, int ops)
        {
            base.SetTypeShardSetting(IndexProductSetting.GetIndexName(), seconds, durability, ops);
            return this;
        }

        public IExecutor OptimizeIndex()
        {
            base.SetOptimize(IndexProductSetting.GetIndexName());
            return this;
        }

        public new IExecutor Output()
        {
            base.Output();
            return this;
        }
    }
}
