*.專案結構
logstash / 匯入設置檔和範例資料
elastic.domain / Common Domain Model 
elastic.domain.b2c / B2C 與EU相關的
elastic.infra / 基礎類別
elastic.task.dataprovider / 資料維護排程, 匯入資料,刪除資料
elastic.web.backend / 維護後台, 同義字,關鍵字維護
elastic.web.dicts / 分詞服務, 回傳現有分詞內容
elastic.web.search / 搜尋服務

*.組態設定
app.setting & web.config 注意大小寫
依EU網站不同,需要改[ELK_SITE]設定值
依Client Node不同,需要改[ELK_ElasticsearchUrl]設定值
<add key="ELK_LocalDataFile" value="E:\elk\b2c\zSch_temp_DT\" /> 檔名會抓ELK_SITE設定值,副檔名會是*.TXT
<add key="ELK_Category_FilePath"/> 內容為完整實體路徑
<add name="FileCache" type="Elastic.Infra.FileCacheProvider" cachePath="~/elk.Search.Cache"/> 快取儲存路徑,必須先建立

*.搜尋服務系統 elastic.web.search 
先確定搜尋的INDEX規則是否有存在的INDEX
目前是使用別名來查詢

*.異動匯入資料欄位需修改以下檔案 ( 以商品資料為例 )
Elastic.Domain.XXXX\Product\Product.cs 匯入的原始資料欄位&格式轉換
Elastic.Domain.XXXX\Product\ProductMapping.cs 對應至ES的MAPPING設定
Elastic.Domain.XXXX\Product\ProductResponse.cs 搜尋時的回應物件
Elastic.Web.Search\Areas\XXXX\Models\ProductResult.cs 回應VIEWMODEL

*.上版事宜
修改AssemblyInfo.cs的版本資訊,配合 localhost/portal/version 可以看BUILD的版本