﻿using Elastic.Domain;

namespace Elastic.Application.UMall
{
    public interface IProductResponseValidator
    {
        bool IsValid(Domain.UMall.Product.Search.ProductResponse request);
    }
}
