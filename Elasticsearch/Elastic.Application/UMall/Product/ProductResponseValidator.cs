﻿using Elastic.Application.DTO;
using Elastic.Infra;

namespace Elastic.Application.UMall.Product
{
    public class ProductResponseValidator : IProductResponseValidator
    {
        protected BasicResult<string> result;

        public ProductResponseValidator() { }

        public bool IsValid(Domain.UMall.Product.Search.ProductResponse response)
        {
            if (response == null) return false;

            return true;
        }
    }
}