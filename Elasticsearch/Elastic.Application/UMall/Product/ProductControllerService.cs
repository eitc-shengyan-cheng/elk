﻿using Elastic.Application.DTO;
using Elastic.Application.Utils;
using Elastic.Domain.UMall.History.Search.Suggest;
using Elastic.Domain.UMall.Product;
using Elastic.Infra;
using System.Threading.Tasks;

namespace Elastic.Application.UMall
{
    public class ProductControllerService : IProductControllerService
    {
        public async Task<Domain.UMall.Product.Search.ProductResponse> SearchForAsync(ProductSearchRequest command)
        {
            var dsl = new ProductRepository().GetSearchText(command.Tokens, ProductFilter.GetFields(command.Tokens), command.Filter, command.Sort, command.PriceRangeGte, command.PriceRangeLte, command.CategoryID, command.Source, (command.PageIndex - 1) * command.PageSize, command.PageSize);

            var resource = string.Format("{0}/_search", IndexProductSetting.GetIndexAliase());
            var response = await RestSharpUtils.PostJsonParameterAsync(resource, dsl);
            var result = ResponseUtils.ConvertForUMall(command, response.Content);

            if (IndexProductSetting.EnableInsertHistory())
                new HistoryControllerService().Insert(command.Account, command.Tokens, command, ( ( result == null ) ? 0 : result.hits.total));

            return result;
        }

        /// <summary>
        /// 以 RestSharp Sync方式執行搜尋
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Domain.UMall.Product.Search.ProductResponse SearchFor(ProductSearchRequest command)
        {
            var dsl = new ProductRepository().GetSearchText(command.Tokens, ProductFilter.GetFields(command.Tokens), command.Filter, command.Sort, command.PriceRangeGte, command.PriceRangeLte, command.CategoryID, command.Source, (command.PageIndex - 1) * command.PageSize, command.PageSize);

            var resource = string.Format("{0}/_search", IndexProductSetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource,dsl);
            var result = ResponseUtils.Convert<Domain.UMall.Product.Search.ProductResponse>(response.Content);

            if (IndexProductSetting.EnableInsertHistory())
                new HistoryControllerService().Insert(command.Account, command.Tokens, command, ((result == null) ? 0 : result.hits.total));

            return result;
        }

        /// <summary>
        /// 以品號查詢商品
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Domain.UMall.Product.Search.ProductSingleResponse SearchForId(string id)
        {
            var resource = string.Format("{0}/{1}/{2}", IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetTypeAliase(), id);
            var response = RestSharpUtils.GetJson(resource);

            return ResponseUtils.ConvertSingle<Domain.UMall.Product.Search.ProductSingleResponse>(response.Content);
        }

        /// <summary>
        /// 更新商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public MethodResult<string> UpdateFor(Elastic.Domain.UMall.Product.Data.Product product)
        {
            var resource = string.Format("{0}/{1}/{2}", IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetTypeAliase(),  product.GOOD_ID);
            return RestSharpUtils.PutJsonBody(resource, product, "UMall.ProductControllerService.UpdateFor"); 
        }

        public HistorySuggestResponse GetSuggestBy(string text)
        {
            var dsl = new ProductRepository().GetSuggestJson(text);
            var resource = string.Format("{0}/_suggest", IndexProductSetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource, dsl);

            return ResponseUtils.Convert<HistorySuggestResponse>(response.Content);
        }
    }
}
