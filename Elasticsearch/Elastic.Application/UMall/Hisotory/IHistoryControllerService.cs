﻿using Elastic.Application.DTO;
using Elastic.Domain.UMall.History.Search.Rank;
using Elastic.Domain.UMall.History.Search.Suggest;
 

namespace Elastic.Application.UMall
{
    public interface IHistoryControllerService
    {
        HistoryRankResponse GetUserBy(HistoryRequest command);

        HistoryRankResponse GetRankBy(HistoryRequest command);

        void Insert(string account, string token, dynamic json, int count);

        HistorySuggestResponse GetSuggestBy(string command);
    }
}
