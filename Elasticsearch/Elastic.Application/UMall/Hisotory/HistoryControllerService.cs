﻿using Elastic.Application.DTO;
using Elastic.Application.Utils;
using Elastic.Domain.UMall.History;
using Elastic.Domain.UMall.History.Search.Rank;
using Elastic.Domain.UMall.History.Search.Suggest;
using System;

namespace Elastic.Application.UMall
{
    /// <summary>
    /// 取得搜尋記錄
    /// http://localhost:5946/umall/history
    /// </summary>
    public class HistoryControllerService : IHistoryControllerService
    {
        public HistoryRankResponse GetUserBy(HistoryRequest command)
        {
            var dsl = new HistoryRepository().GetUserJson(command.User, command.DateRangeGte, command.DateRangeLte, command.Size);
            var resource = string.Format("{0}/_search", IndexHistorySetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource, dsl);

            return ResponseUtils.Convert<HistoryRankResponse>(response.Content);
        }

        public HistoryRankResponse GetRankBy(HistoryRequest command)
        {
            var dsl = new HistoryRepository().GetRankJson(command.DateRangeGte, command.DateRangeLte, command.Size);
            var resource = string.Format("{0}/_search", IndexHistorySetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource, dsl);

            return ResponseUtils.Convert<HistoryRankResponse>(response.Content);
        }

        public void Insert(string account, string token, dynamic json, int count)
        {
            var dsl = new HistoryRepository().GetInsertEntity(account, token, json, DateTime.Now, count);
            var resource = string.Format("{0}/history", IndexHistorySetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonBodyAsync(resource, dsl);
        }

        public HistorySuggestResponse GetSuggestBy(string text)
        {
            var dsl = new HistoryRepository().GetSuggestJson(text);
            var resource = string.Format("{0}/_suggest", IndexHistorySetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource, dsl);

            return ResponseUtils.Convert<HistorySuggestResponse>(response.Content);
        }
    }
}
