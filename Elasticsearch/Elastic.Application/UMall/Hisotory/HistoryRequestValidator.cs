﻿using Elastic.Application.DTO;
using Elastic.Infra;

namespace Elastic.Application.UMall.History
{
    public class HistoryRequestValidator : IHistoryRequestValidator
    {
        protected BasicResult<string> result;

        public HistoryRequestValidator() { }

        public bool IsValid(HistoryRequest request)
        {
            if (request == null) return false;
            if (request.DateRangeLte == request.DateRangeGte) return false;

            return true;
        }
    }
}