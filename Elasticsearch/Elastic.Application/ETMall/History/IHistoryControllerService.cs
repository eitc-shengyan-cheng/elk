﻿using Elastic.Application.DTO;
using Elastic.Domain.ETMall.History.Search.Rank;
using Elastic.Domain.ETMall.History.Search.Suggest;
using System;

namespace Elastic.Application.ETMall
{
    public interface IHistoryControllerService
    {
        HistoryRankResponse GetUserBy(HistoryRequest command);

        HistoryRankResponse GetRankBy(HistoryRequest command);

        void Insert(string account, string token, dynamic json, int count);

        HistorySuggestResponse GetSuggestBy(string command);
    }
}
