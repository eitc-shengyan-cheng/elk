﻿using Elastic.Application.Utils;
using Elastic.Domain.ETMall.History.Search.Suggest;
using Elastic.Domain.ETMall.Product;
using Elastic.Infra;
using System.Threading.Tasks;

namespace Elastic.Application.ETMall
{
    public class ProductControllerService : IProductControllerService
    {
        public async Task<Domain.ETMall.Product.Search.ProductResponse> SearchForAsync(DTO.ProductSearchRequest command)
        {
            var dsl = new ProductRepository().GetSearchText(command.Tokens, ProductFilter.GetFields(command.Tokens), command.Filter, command.Sort, command.PriceRangeGte, command.PriceRangeLte, command.CategoryID, command.Source, (command.PageIndex - 1) * command.PageSize, command.PageSize);

            var resource = string.Format("{0}/_search", IndexProductSetting.GetIndexAliase());
            var response = await RestSharpUtils.PostJsonParameterAsync(resource, dsl);
            var result = ResponseUtils.ConvertForETMall(command, response.Content);

            if (IndexProductSetting.EnableInsertHistory())
                new HistoryControllerService().Insert(command.Account, command.Tokens, command, ((result == null) ? 0 : result.hits.total));

            return result;
        }

        /// <summary>
        /// 以 RestSharp Sync方式執行搜尋
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Domain.ETMall.Product.Search.ProductResponse SearchFor(DTO.ProductSearchRequest command)
        {
            var dsl = new ProductRepository().GetSearchText(command.Tokens, ProductFilter.GetFields(command.Tokens), command.Filter, command.Sort, command.PriceRangeGte, command.PriceRangeLte, command.CategoryID, command.Source, (command.PageIndex - 1) * command.PageSize, command.PageSize);

            var resource = string.Format("{0}/_search", IndexProductSetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource, dsl);
            var result = ResponseUtils.Convert<Domain.ETMall.Product.Search.ProductResponse>(response.Content);

            if (IndexProductSetting.EnableInsertHistory())
                new HistoryControllerService().Insert(command.Account, command.Tokens, command, ((result == null) ? 0 : result.hits.total));

            return result;
        }

        /// <summary>
        /// 以品號查詢商品
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Domain.ETMall.Product.Search.ProductSingleResponse SearchForId(string id)
        {
            var resource = string.Format("{0}/{1}/{2}", IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetTypeAliase(), id);
            var response = RestSharpUtils.GetJson(resource);

            return ResponseUtils.ConvertSingle<Domain.ETMall.Product.Search.ProductSingleResponse>(response.Content);
        }

        public MethodResult<string> UpdateFor(Domain.ETMall.Product.Data.Product product)
        {
            var resource = string.Format("{0}/{1}/{2}", IndexProductSetting.GetIndexAliase(), IndexProductSetting.GetTypeAliase(), product.GOOD_ID);
            return RestSharpUtils.PutJsonBody(resource, product , "ETMall.ProductControllerService.UpdateFor");
        }

        public HistorySuggestResponse GetSuggestBy(string text)
        {
            var dsl = new ProductRepository().GetSuggestJson(text);
            var resource = string.Format("{0}/_suggest", IndexProductSetting.GetIndexAliase());
            var response = RestSharpUtils.PostJsonParameter(resource, dsl);

            return ResponseUtils.Convert<HistorySuggestResponse>(response.Content);
        }
    }
}
