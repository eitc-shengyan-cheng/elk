﻿using Elastic.Application.DTO;
using Elastic.Infra;

namespace Elastic.Application.ETMall.Product
{
    public class ProductRequestValidator : IProductRequestValidator
    {
        protected BasicResult<string> result;

        public ProductRequestValidator() { }

        public bool IsValid(ProductSearchRequest request)
        {
            if (request == null) return false;
            if (request.Tokens == string.Empty) return false;
            if (request.PriceRangeLte == request.PriceRangeGte) return false;

            return true;
        }

        public bool IsGoodIdFormat(string input)
        {
            int id;
            if (!int.TryParse(input, out id)) return false;
            if (input.Trim().Length < 6 || input.Trim().Length > 8) return false;

            return true;
        }
    }
}