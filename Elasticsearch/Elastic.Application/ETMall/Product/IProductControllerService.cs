﻿using Elastic.Application.DTO;
using Elastic.Domain.ETMall.History.Search.Suggest;
using Elastic.Domain.ETMall.Product.Search;
using Elastic.Infra;
using System.Threading.Tasks;

namespace Elastic.Application.ETMall
{
    public interface IProductControllerService
    {
        /// <summary>
        /// 非同步搜尋商品資料,返回多筆
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Task<ProductResponse> SearchForAsync(ProductSearchRequest command);
        /// <summary>
        /// 搜尋商品資料,返回多筆
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        ProductResponse SearchFor(ProductSearchRequest command);
        /// <summary>
        /// 依ID搜尋商品資料,返回單筆
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProductSingleResponse SearchForId(string id);
        /// <summary>
        /// 更新單一商品資料
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        MethodResult<string> UpdateFor(Domain.ETMall.Product.Data.Product product);

        HistorySuggestResponse GetSuggestBy(string command);
    }
}
