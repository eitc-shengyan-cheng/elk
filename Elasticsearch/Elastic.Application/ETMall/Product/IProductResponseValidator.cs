﻿using Elastic.Domain;

namespace Elastic.Application.ETMall
{
    public interface IProductResponseValidator
    {
        bool IsValid(Domain.ETMall.Product.Search.ProductResponse request);
    }
}
