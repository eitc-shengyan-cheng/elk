﻿using Elastic.Application.DTO;
using Elastic.Infra;

namespace Elastic.Application.ETMall.Product
{
    public class ProductResponseValidator : IProductResponseValidator
    {
        protected BasicResult<string> result;

        public ProductResponseValidator() { }

        public bool IsValid(Domain.ETMall.Product.Search.ProductResponse response)
        {
            if (response == null) return false;
            if (response.hits == null ) return false;
            
            return true;
        }
    }
}