﻿using Elastic.Application.DTO;
using Elastic.Domain;
using Elastic.Domain.ETMall.Product;
using Elastic.Infra;
using RestSharp;
using System.Linq;
using System.Threading.Tasks;

namespace Elastic.Application
{
    /// <summary>
    /// testing
    /// </summary>
    public class ProductRepositoryFactory
    {
        internal IProductRepository CreateInstance(string name)
        {
            switch (name)
            {
                case "ETMall":
                    return new Elastic.Domain.ETMall.Product.ProductRepository();
                case "UMall":
                    return new Elastic.Domain.UMall.Product.ProductRepository();
                default:
                    return new Elastic.Domain.ETMall.Product.ProductRepository();
            }
        }

        /*
        public Domain.ProductResponse SearchForAsync<ProductResponseX, R>(ProductRequest command)  
        {
            var dsl = new Domain.UMall.Product.ProductRepository().GetSearchText(command.Tokens, ProductFilter.GetFields(command.Tokens), command.Filter, command.Sort, command.PriceRangeGte, command.PriceRangeLte);
            var response = RestSharpUtils.SearchForAsync(dsl, IndexProductSetting.GetIndexAliase());


            return GetResult(command, response);
        }

        public static ProductResponse GetResult(ProductRequest command, Task<IRestResponse> response)  
        {
            if (!SerializeUtils.IsPropertyExist(response.Result.Content, "error"))
            {
                //dynamic obj = SerializeUtils.DeserializeDynamic(response.Result.Content);
                
                Elastic.Domain.ProductResponse b = new ProductResponse();
                //b.hits.hits[0]._source = new Elastic.Domain._source();              
                
                ProductResponse obj = SerializeUtils.Deserialize<Domain.ProductResponse>(response.Result.Content);

                if (obj.hits.total > 0)
                {
                    obj.hits.hits = obj.hits.hits.Skip((command.PageIndex - 1) * command.PageSize).Take(command.PageSize).ToArray();
                    return obj;
                }
                else
                {
                    return obj;
                }
            }
            else
            {
                //error handling
                return null;
            }
        }
        */
    }
}
