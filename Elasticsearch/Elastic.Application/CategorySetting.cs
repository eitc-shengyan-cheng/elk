﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Elastic.Application
{
    public class CategorySetting
    {
        public static Dictionary<int, Category> Categorys { get; private set; }

        private CategorySetting() { }

        public static void GetCategorys(string physicalApplicationPath)
        {
            var categoryList = File.ReadAllLines(Path.Combine(physicalApplicationPath, ConfigurationManager.AppSettings["ELK_Category_FilePath"], ElasticSetting.GetSite() + ".category.txt"), Encoding.Default);

            Categorys = new Dictionary<int, Category>();

            foreach (var categoryItem in categoryList)
            {
                var x = categoryItem.Split(',');

                Category category = new Category();
                if (x.Length == 4)
                {
                    category.CATE_NAME = x[0];
                    category.CATE_ID = Int32.Parse(x[1]);
                    category.PARENT_ID = Int32.Parse(x[2]);
                    category.LEVEL = Int32.Parse(x[3]);
                }
                else
                {
                    //Category Name 有可能包含',' ex. 5,000以下,39472,39470,3
                    category.CATE_NAME = string.Join(",", x.Take(x.Length - 3));
                    category.CATE_ID = Int32.Parse(x[x.Length - 3]);
                    category.PARENT_ID = Int32.Parse(x[x.Length - 2]);
                    category.LEVEL = Int32.Parse(x[x.Length - 1]);
                }

                if (!Categorys.ContainsKey(category.CATE_ID))
                {
                    Categorys.Add(category.CATE_ID, category);
                }

            }

            //Categorys = categorys.Select(x => x.Split(','))
            //    .ToDictionary(x => Int32.Parse(x[1]), x => new Category { CATE_NAME = x[0], CATE_ID = Int32.Parse(x[1]), PARENT_ID = Int32.Parse(x[2]), LEVEL = Int32.Parse(x[3]) });
        }
    }

    public class Category
    {
        public string CATE_NAME { get; set; }
        public int CATE_ID { get; set; }
        public int PARENT_ID { get; set; }
        public int LEVEL { get; set; }
    }
}
