﻿using System;
using System.Configuration;

namespace Elastic.Application
{
    public static class IndexHistorySetting
    {
        public static string GetIndexAliase()
        {
            return ConfigurationManager.AppSettings["ELK_Site"] + "." + ConfigurationManager.AppSettings["ELK_History_Aliase"];
        }
    }
}
