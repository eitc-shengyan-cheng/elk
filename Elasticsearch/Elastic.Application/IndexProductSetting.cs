﻿using System;
using System.Configuration;

namespace Elastic.Application
{
    public static class IndexProductSetting
    {
        /// <summary>
        /// 目標網站 ( ETMALL or UMALL )
        /// </summary>
        public static string GetSite()
        {
            return ConfigurationManager.AppSettings["ELK_Site"];
        }

        /// <summary>
        /// highlightName
        /// </summary>
        public static string GetHighLightName()
        {
            return "marked";
        }

        public static string GetIndexAliase()
        {
            return ConfigurationManager.AppSettings["ELK_Site"] + "." + ConfigurationManager.AppSettings["ELK_Product_Aliase"];
        }

        public static string GetTypeAliase()
        {
            return ConfigurationManager.AppSettings["ELK_Product_Aliase"];
        }

        /// <summary>
        /// 取得商品INDEX名稱
        /// </summary>
        /// <returns></returns>
        public static string GetIndexName()
        {
            if (IsIndexAppendDateFormat())
                return string.Format("{0}.{1}", GetIndexNameSetting(), DateToday);
            else
                return GetIndexNameSetting();
        }

        /// <summary>
        /// For Importer Remove Aliases
        /// </summary>
        /// <returns></returns>
        public static string GetLastIndexName()
        {
            if (IsIndexAppendDateFormat())
                return string.Format("{0}.{1}", GetIndexNameSetting(), DateYestoDay);
            else
                return GetIndexNameSetting();
        }

        /// <summary>
        /// For ImporterRemove Index
        /// </summary>
        /// <returns></returns>
        public static string GetIndexRemoveName()
        {
            int day = int.Parse(ConfigurationManager.AppSettings["ELK_Product_Index_RemoveDay"].ToString());

            return String.Format("{0}.{1:yyyyMMdd}", GetIndexNameSetting(), DateTime.Now.AddDays(-day));
        }

        /// <summary>
        /// 取得系統商品INDEX名稱
        /// </summary>
        /// <returns></returns>
        private static string GetIndexNameSetting()
        {
            return ConfigurationManager.AppSettings["ELK_Site"] + "." + ConfigurationManager.AppSettings["ELK_Product_Index"];
        }

        /// <summary>
        /// 取得商品TYPE名稱
        /// </summary>
        public static string GetTypeName()
        {
            return ConfigurationManager.AppSettings["ELK_Product_Index_Type"];
        }

        /// <summary>
        /// 是否在INDEX後加上當天日期格式
        /// </summary>
        private static bool IsIndexAppendDateFormat()
        {
            return bool.Parse(ConfigurationManager.AppSettings["ELK_Product_Index_Append_DateFormat"]);
        }

        /// <summary>
        /// 當天日期格式
        /// </summary>
        private static string DateToday = String.Format("{0:yyyyMMdd}", DateTime.Now);

        /// <summary>
        /// 昨天日期格式
        /// </summary>
        private static string DateYestoDay = String.Format("{0:yyyyMMdd}", DateTime.Now.AddDays(-1));

        public static bool EnableInsertHistory()
        {
            return bool.Parse(ConfigurationManager.AppSettings["ELK_History_Enable"]);
        }
    }
}
