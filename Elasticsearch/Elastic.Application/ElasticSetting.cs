﻿using System;
using System.Configuration;

namespace Elastic.Application
{
    public static class ElasticSetting
    {
        public static string Host = ConfigurationManager.AppSettings["ELK_ElasticsearchUrl"];

        /// <summary>
        /// 目標網站 ( ETMALL or UMALL )
        /// </summary>
        public static string GetSite()
        {
            return ConfigurationManager.AppSettings["ELK_Site"];
        }
    }
}
