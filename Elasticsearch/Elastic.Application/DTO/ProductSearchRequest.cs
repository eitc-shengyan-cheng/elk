﻿using System.Collections.Generic;

namespace Elastic.Application.DTO
{
    /// <summary>
    /// 商品搜尋REQUEST
    /// </summary>
    public class ProductSearchRequest
    {
        /// <summary>
        /// 搜尋帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 關鍵字詞
        /// </summary>
        public string Tokens { get; set; }

        /// <summary>
        /// 價格起始值
        /// </summary>
        public int PriceRangeGte { get; set; }

        /// <summary>
        /// 價格結束值
        public int PriceRangeLte { get; set; }

        /// <summary>
        /// 過濾的欄位
        /// 館別:[KEY:VALUE]
        /// 館別:[CategoryStore:視聽家電]
        /// 
        /// 商品來源:[KEY:VALUE]
        /// 商品來源:旅遊.TravelProduct 
        /// 商品來源:成人.AdultProduct 
        /// 
        /// 其他條件:[KEY:VALUE]
        /// 其他條件:快速到貨.OneDayShip
        /// 其他條件:超商取貨.StoreService 
        /// 其他條件:可使用折價券.Coupon
        /// 其他條件:促銷商品.DISCOUNT_VALUE
        /// </summary>
        public IDictionary<string,string> Filter { get; set; }

        /// <summary>
        /// 排序的欄位
        /// 最熱門 HOTVIEW	 
        /// 最熱銷 HOTSALE  
        /// 價格 PRC (東森價)
        /// 最新 OnlineDateTime?
        /// 精準 _score?
        /// </summary>
        public IDictionary<string, string> Sort { get; set; }

        /// <summary>
        /// 頁數
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 頁面呈現商品數量
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 搜尋分類等級 (0,1,2,3)
        /// </summary>
        public int CategoryLevel { get; set; }

        /// <summary>
        /// 搜尋分類編號
        /// </summary>
        public int CategoryID { get; set; }

        /// <summary>
        /// Elasticsearch 回傳的_SOURCE要有哪些欄位
        /// </summary>
        public IList<string> Source { get; set; }

        public ProductSearchRequest()
        {
            this.PageSize = 10;
            this.PageIndex = 1;
            this.PriceRangeGte = 0;
            this.PriceRangeLte = 100000000;
            this.Source = new List<string>() { "GOOD_ID", "GOOD_NM", "Description", "Specifications", "CategorysValue", "DISCOUNT_VALUE", "PRC" };
        }
    }
}
