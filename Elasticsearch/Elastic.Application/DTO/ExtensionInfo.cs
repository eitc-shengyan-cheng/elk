﻿namespace Elastic.Application
{
    public class ExtensionInfo
    {
        /// <summary>
        /// Elasticsearch Url
        /// </summary>
        public string ES { get; set; }

        /// <summary>
        /// Application Server Name
        /// </summary>
        public string S { get; set; }

        /// <summary>
        /// Execute Time
        /// </summary>
        public int Took { get; set; }
    }
}
