﻿namespace Elastic.Application.DTO
{
    public interface IProductRequestValidator
    {
        bool IsValid(ProductSearchRequest request);

        bool IsGoodIdFormat(string input);
    }
}
