﻿using System.Collections.Generic;

namespace Elastic.Application.DTO
{
    /// <summary>
    /// 商品更新參數
    /// </summary>
    public class ProductUpdateRequest<T>
    {
        /// <summary>
        /// 搜尋帳號
        /// </summary>
        public string Account { get; set; }

        public List<ProductUpdateRequestItem<T>> Items { get; set; }
    }

    /// <summary>
    /// 商品更新參數
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ProductUpdateRequestItem<T>
    {
        public string GOOD_ID { get; set; }
        public T PRC { get; set; }
        public string GOOD_NM { get; set; }
        public int COUPON { get; set; }
        public int DISCOUNT_VALUE { get; set; }
        public int STORESERVICE { get; set; }
        public int ONEDAYSHIP { get; set; }
        public int ONLINE { get; set; }
    }
}
