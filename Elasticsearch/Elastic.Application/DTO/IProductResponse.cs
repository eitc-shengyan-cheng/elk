﻿using System.Collections;

namespace Elastic.Application.DTO
{
    public interface IProductResponse
    {
        /// <summary>
		/// 執行結果代碼
		/// </summary>
		int Code { get; set; }
        /// <summary>
        /// 執行結果說明
        /// </summary>
        string Desc { get; set; }
        /// <summary>
        /// 查詢結果
        /// </summary>
        /// <returns></returns>		
        dynamic Result { get; set; }
        /// <summary>
        /// 查詢總筆數
        /// </summary>
        int Count { get; set; }
        /// <summary>
        /// 總頁數
        /// </summary>
        int Pages { get; set; }
        /// <summary>
        /// 取得分類限縮清單
        /// </summary>
        IList Categorys { get; }
        /// <summary>
        /// 取得預設限縮清單
        /// </summary>
        IList Predefined { get; }
        /// <summary>
        /// 相關字詞
        /// </summary>
        string[] Terms { get; set; }

        /// <summary>
        /// 回應的主機資訊
        /// </summary>
        ExtensionInfo Info { get; set; }
    }
}
