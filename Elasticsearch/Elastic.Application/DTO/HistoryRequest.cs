﻿using System;

namespace Elastic.Application.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class HistoryRequest
    {
        /// <summary>
        /// 關鍵字詞
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// 查詢日期起始值
        /// </summary>
        public DateTime DateRangeGte { get; set; }

        /// <summary>
        /// 查詢日期結束值
        public DateTime DateRangeLte { get; set; }

        public int Size { get; set; }

        public HistoryRequest()
        {
            this.Size = 0;
            this.DateRangeGte = DateTime.Now.AddDays(-30);
            this.DateRangeLte = DateTime.Now;
        }
    }
}
