﻿namespace Elastic.Application.DTO
{
    public interface IHistoryRequestValidator
    {
        bool IsValid(HistoryRequest request);
    }
}
