﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Elastic.Application.Utils
{
    public static class HttpContextUtils
    {
        public static string PhysicalApplicationPath()
        {
            if ( HttpContext.Current == null )
                return @"E:\Git\ELK\Elasticsearch\Elastic.Web.Search\";
            else
                return HttpContext.Current.Request.PhysicalApplicationPath;
        }
    }
}
