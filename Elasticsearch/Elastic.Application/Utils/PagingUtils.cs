﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elastic.Application.Utils
{
    public static class PagingUtils
    {
            public static int GetPageIndex(int count, int pageSize)
            {
                return Convert.ToInt32(Math.Ceiling(Convert.ToDouble(count) / Convert.ToDouble(pageSize)));
            }
    }
}
