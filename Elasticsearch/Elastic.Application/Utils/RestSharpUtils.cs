﻿using Elastic.Infra;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace Elastic.Application
{
    public static class RestSharpUtils
    {
        public static RestClient client = new RestClient();

        public static IRestResponse PostJsonParameter(string resource, string dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);

            RestRequest request = new RestRequest(resource, Method.POST);
            request.AddParameter("text/json", dsl, ParameterType.RequestBody);

            return client.Execute(request);
        }

        public static IRestResponse PostJsonBody(string resource, object dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);
            RestRequest request = new RestRequest(resource, Method.POST);

            if (dsl != null)
            {
                request.RequestFormat = DataFormat.Json;
                request.AddBody(dsl);
            }

            return client.Execute(request);
        }

        public static async Task<IRestResponse> PostJsonBodyAsync(string resource, object dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);
            RestRequest request = new RestRequest(resource, Method.POST);

            if (dsl != null)
            {
                request.RequestFormat = DataFormat.Json;
                request.AddBody(dsl);
            }

            Task<IRestResponse> task = client.ExecuteTaskAsync(request);
            return await task;
        }

        public static async Task<IRestResponse> PostJsonParameterAsync(string resource, string dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);
            RestRequest request = new RestRequest(resource, Method.POST);

            request.AddParameter("text/json", dsl, ParameterType.RequestBody);

            Task<IRestResponse> task = client.ExecuteTaskAsync(request);
            return await task;
        }

        public static IRestResponse GetJson(string resource)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);

            RestRequest request = new RestRequest(resource, Method.GET);
            return client.Execute(request);
        }

        public static IRestResponse PutJsonBody(string resource, object dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);

            RestRequest request = new RestRequest(resource, Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(dsl);

            return client.Execute(request);
        }

        public static IRestResponse PutJsonParameter(string resource, object dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);

            RestRequest request = new RestRequest(resource, Method.PUT);
            request.AddParameter("text/json", dsl, ParameterType.RequestBody);

            return client.Execute(request);
        }

        public static IRestResponse DeleteJsonParameter(string resource, object dsl)
        {
            client.BaseUrl = new Uri(ElasticSetting.Host);
            RestRequest request = new RestRequest(resource, Method.DELETE);
            request.AddParameter("text/json", dsl, ParameterType.RequestBody);

            return client.Execute(request);
        }

        public static MethodResult<string> GetResult(string resource, IRestResponse response, object dsl, string name)
        {
            return (new MethodResult<string>()
            {
                Command = dsl,
                Parameter = resource,
                Name = name,
                Data = response.Content.ToString(),
                Message = response.StatusCode.ToString(),
                HasError = response.Content.Contains("error")
            });
        }
        
        public static MethodResult<string> PostJsonBody(string resource, object dsl, string name = "default")
        {
            var response = PostJsonBody(resource, dsl);
            return GetResult(resource, response, dsl, name);
        }

        public static MethodResult<string> PostJsonParameter(string resource, string dsl, string name = "default")
        {
            var response = PostJsonParameter(resource, dsl);
            return GetResult(resource, response, dsl, name);
        }

        public static MethodResult<string> GetJsonParameter(string resource, string name = "default")
        {
            var response = GetJson(resource);
            return GetResult(resource, response, "empty", name);
        }

        public static MethodResult<string> PutJsonBody(string resource, object dsl, string name = "default")
        {
            var response = PutJsonBody(resource, dsl);
            return GetResult(resource, response, dsl, name);
        }

        public static MethodResult<string> PutJsonParameter(string resource, object dsl, string name = "default")
        {
            var response = PutJsonParameter(resource, dsl);
            return GetResult(resource, response, dsl, name);
        }          

        public static MethodResult<string> DeleteJsonParameter(string resource, object dsl = null, string name = "default")
        {
            var response = DeleteJsonParameter(resource, dsl);
            return GetResult(resource, response, dsl, name);
        }
    }
}
