﻿using Elastic.Application.DTO;
using Elastic.Infra;
using RestSharp;
using System.Linq;
using System.Threading.Tasks;

namespace Elastic.Application.Utils
{
    public static class ResponseUtils
    {
        public static T Convert<T>(string response)
        {
            if (!response.Contains("\"error\":"))
            {
                if (response.Contains("\"total\":0"))
                {
                    return default(T);
                }
                else
                {
                    return SerializeUtils.Deserialize<T>(response);
                }
            }
            else
            {
                //error handling
                return default(T);
            }
        }

        public static T ConvertSingle<T>(string response)
        { 
            if (response.Contains("\"found\":true"))
            {
                return SerializeUtils.Deserialize<T>(response);
            }
            else
            {
                //error handling
                return default(T);
            }
        }

        public static Domain.ETMall.Product.Search.ProductResponse ConvertForETMall(ProductSearchRequest command, string content)
        {
            if (!content.Contains("\"error\":"))
            {
                var obj = SerializeUtils.Deserialize<Domain.ETMall.Product.Search.ProductResponse>(content);

                if (obj.hits.total > 0)
                {
                    obj.hits.hits = obj.hits.hits.Skip((command.PageIndex - 1) * command.PageSize).Take(command.PageSize).ToArray();
                    return obj;
                }
                else
                {
                    return new Domain.ETMall.Product.Search.ProductResponse()
                    {
                        timed_out = obj.timed_out,
                        took = obj.took
                    };
                }
            }
            else
            {
                //error handling
                return null;
            }
        } 

        public static Domain.UMall.Product.Search.ProductResponse ConvertForUMall(ProductSearchRequest command, string content)
        {
            if (!content.Contains("\"error\":"))
            {
                var obj = SerializeUtils.Deserialize<Domain.UMall.Product.Search.ProductResponse>(content);

                if (obj.hits.total > 0)
                {
                    obj.hits.hits = obj.hits.hits.Skip((command.PageIndex - 1) * command.PageSize).Take(command.PageSize).ToArray();
                    return obj;
                }
                else
                {
                    return new Domain.UMall.Product.Search.ProductResponse()
                    {
                        timed_out = obj.timed_out,
                        took = obj.took
                    };
                }
            }
            else
            {
                //error handling
                return null;
            }
        }
    }
}
